use shared::parse_numbers;

fn main() {
    let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let total = input.lines().fold(0, |total, line| {
        let report = parse_numbers::<isize>(line);
        // print!("\n{:?}", nums);
        if safe(report) {
            total + 1
        } else {
            total
        }
    });

    println!("Part 1 - {}", total);

    let total2 = input.lines().fold(0, |total, line| {
        let report = parse_numbers::<isize>(line);
        // print!("\n{:?}", nums);
        for i in 0..report.len() {
            let mut adjusted = report.clone();
            adjusted.remove(i);

            if safe(adjusted) {
                return total + 1;
            }
        }
        return total;
    });

    println!("Part 2 - {}", total2);
}

fn safe(report: Vec<isize>) -> bool {
    let increasing = report[0] < report[1];
    for pair in report.windows(2) {
        // unsafe if inconsistent direction
        if increasing {
            if pair[0] >= pair[1] {
                return false;
            }
        } else {
            if pair[0] <= pair[1] {
                return false;
            }
        }
        // unsafe if too far
        if (pair[0] - pair[1]).abs() > 3 {
            return false;
        }
    }
    return true;
}
