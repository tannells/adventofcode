use std::iter::zip;
use shared::parse_numbers;

fn main() {
    let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let mut left: Vec<isize> = Vec::new();
    let mut right: Vec<isize> = Vec::new();

    input.lines().for_each(|line| {
        let nums = parse_numbers(line);
        left.push(nums[0]);
        right.push(nums[1]);
    });

    left.sort();
    right.sort();

    let total_distance = zip(left.iter(), right.iter()).fold(0, |sum, (a, b)| sum + (a - b).abs());

    println!("Part 1 - {}", total_distance);

    let similarity_score = left.iter().fold(0, |score, &l| {
        let r_count = right.iter().filter(|&&r| l == r).count();
        score + l * isize::try_from(r_count).unwrap()
    });
    println!("Part 2 - {}", similarity_score);
}
