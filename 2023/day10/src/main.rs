use std::collections::BTreeSet;

fn main() {
    // let input = include_str!("../example.txt");
    // let input = include_str!("../example2.txt");
    let input = include_str!("../params.txt");

    let mut grid = Grid::parse(input);
    let loc = grid.starting_location();

    println!("Part 1 - {}", grid.flood());
    // grid.print_steps();
    // grid.print_grid();
    
    println!("Part 2 - {}", grid.count_inside());

    // grid.print_grid();
}

#[derive(Debug)]
struct Grid {
    grid: Vec<Vec<char>>,
    rows: usize,
    cols: usize,
    steps: Vec<Vec<i32>>,
}

impl Grid {
    fn parse(input: &str) -> Grid {
        let grid: Vec<Vec<char>> = input.lines().map(|line| line.chars().collect()).collect();
        let rows = grid.len();
        let cols = grid[0].len();
        let steps = vec![vec![-1; cols]; rows];

        Grid {
            grid: grid,
            rows: rows,
            cols: cols,
            steps: steps,
        }
    }

    fn starting_location(&self) -> Location {
        for y in 0..self.rows {
            for x in 0..self.cols {
                if self.get(x, y) == 'S' {
                    return Location { x: x, y: y };
                }
            }
        }
        return Location { x: 0, y: 0 };
    }

    fn get(&self, x: usize, y: usize) -> char {
        self.grid[y][x]
    }

    fn get_l(&self, loc: &Location) -> char {
        self.grid[loc.y][loc.x]
    }

    fn steps_to(&self, x: usize, y: usize) -> i32 {
        self.steps[y][x]
    }

    fn steps_to_l(&self, location: &Location) -> i32 {
        self.steps[location.y][location.x]
    }

    fn adjacent(&self, from: &Location, to: &Location) -> bool {
        let dir = from.dir_to(to);
        let from_pipe = Pipe::from(self.get_l(from));
        let to_pipe = Pipe::from(self.get_l(to));
        from_pipe.connects_to(&dir, &to_pipe)
    }

    fn adjacent_locations(&self, location: &Location) -> Vec<Location> {
        let mut adjacent_locations = Vec::new();
        if location.x > 0 && self.adjacent(location, &location.west()) {
            adjacent_locations.push(location.west())
        }
        if location.x < self.cols - 1 && self.adjacent(location, &location.east()) {
            adjacent_locations.push(location.east())
        }
        if location.y > 0 && self.adjacent(location, &location.north()) {
            adjacent_locations.push(location.north())
        }
        if location.y < self.rows - 1 && self.adjacent(location, &location.south()) {
            adjacent_locations.push(location.south())
        }
        adjacent_locations
    }

    fn flood(&mut self) -> i32 {
        let mut edge_locations: BTreeSet<Location> = BTreeSet::new();
        edge_locations.insert(self.starting_location());
        let mut steps = 0;
        let mut next_edges: BTreeSet<Location> = BTreeSet::new();
        loop {
            next_edges.clear();

            for edge in &edge_locations {
                self.steps[edge.y][edge.x] = steps;
            }
            for edge in &edge_locations {
                let mut new_edges = self
                    .adjacent_locations(&edge)
                    .into_iter()
                    .filter(|adjacent| self.steps_to_l(adjacent) == -1)
                    .collect();
                next_edges.append(&mut new_edges);
            }
            if (next_edges.is_empty()) {
                return steps;
            }
            edge_locations.clear();
            edge_locations.append(&mut next_edges);
            steps += 1;
        }
    }

    fn count_inside(&mut self) -> i32 {
        let mut outside = true;
        let mut inside_count = 0;
        let mut last_turn = '0';
        for y in 0..self.rows {
            for x in 0..self.cols {
                // check if part of loop
                if self.steps_to(x, y) == -1 {
                    // not part of loop -> replace with O/I, count inside
                    if outside {
                        self.grid[y][x] = 'O';
                    } else {
                        self.grid[y][x] = 'I';
                        inside_count += 1;
                    }
                } else {
                    // part of loop -> check whether crossing loop
                    match (self.get(x, y)) {
                        '|' => outside = !outside,
                        'L' => last_turn = 'L',
                        'J' => {
                            if last_turn == 'F' {
                                outside = !outside
                            }
                        }
                        '7' => {
                            if last_turn == 'L' {
                                outside = !outside
                            }
                        }
                        'F' => last_turn = 'F',
                        _ => {}
                    }
                }
            }
        }
        inside_count
    }

    fn print_steps(&self) {
        for y in 0..self.rows {
            for x in 0..self.cols {
                print!("{:>6}", self.steps_to(x, y))
            }
            print!("\n")
        }
        print!("\n")
    }

    fn print_grid(&self) {
        for y in 0..self.rows {
            for x in 0..self.cols {
                print!("{}", self.get(x, y))
            }
            print!("\n")
        }
        print!("\n")
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
struct Location {
    x: usize,
    y: usize,
}

impl Location {
    fn from(x: usize, y: usize) -> Location {
        Location { x: x, y: y }
    }

    fn north(&self) -> Location {
        Location {
            x: self.x,
            y: self.y - 1,
        }
    }
    fn east(&self) -> Location {
        Location {
            x: self.x + 1,
            y: self.y,
        }
    }
    fn south(&self) -> Location {
        Location {
            x: self.x,
            y: self.y + 1,
        }
    }
    fn west(&self) -> Location {
        Location {
            x: self.x - 1,
            y: self.y,
        }
    }

    fn dir_to(&self, to: &Location) -> Direction {
        if self.x - 1 == to.x {
            return Direction::West;
        }
        if self.x + 1 == to.x {
            return Direction::East;
        }
        if self.y - 1 == to.y {
            return Direction::North;
        }
        return Direction::South;
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn opposite(&self) -> Direction {
        match self {
            Self::North => Self::South,
            Self::South => Self::North,
            Self::East => Self::West,
            Self::West => Self::East,
        }
    }
}

struct Pipe {
    pipe: char,
}

impl Pipe {
    fn from(pipe: char) -> Pipe {
        Pipe { pipe: pipe }
    }

    fn connects_dir(&self, dir: &Direction) -> bool {
        match self.pipe {
            '|' => match dir {
                Direction::North => true,
                Direction::South => true,
                _ => false,
            },
            '-' => match dir {
                Direction::West => true,
                Direction::East => true,
                _ => false,
            },
            'L' => match dir {
                Direction::North => true,
                Direction::East => true,
                _ => false,
            },
            'J' => match dir {
                Direction::North => true,
                Direction::West => true,
                _ => false,
            },
            '7' => match dir {
                Direction::West => true,
                Direction::South => true,
                _ => false,
            },
            'F' => match dir {
                Direction::East => true,
                Direction::South => true,
                _ => false,
            },
            'S' => true,
            _ => false,
        }
    }

    fn connects_to(&self, dir: &Direction, other: &Pipe) -> bool {
        if self.connects_dir(dir) {
            return other.connects_dir(&dir.opposite());
        }
        false
    }
}
