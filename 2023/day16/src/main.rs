use std::{cmp, collections::BTreeSet, fmt};

fn main() {
    let input = include_str!("../example.txt");
    // let input = include_str!("../params.txt");
    let mut contraption = Grid::parse(input);
    // println!("{:?}", contraption);
    contraption.start_beam((0, 0), &Direction::East);
    // println!("{:?}", contraption);

    println!("Part 1 - {}", contraption.count_energized());

    let max = contraption.find_max_energized();
    println!("Part 2 - {}", max);
}

struct Grid {
    grid: Vec<Vec<char>>,
    energized: Vec<Vec<bool>>,
    traced_beams: BTreeSet<((usize, usize), Direction)>,
}

impl Grid {
    fn parse(input: &str) -> Grid {
        let g: Vec<Vec<char>> =
            input
                .lines()
                .map(|line| line.chars())
                .fold(Vec::new(), |mut grid, line_iter| {
                    line_iter.enumerate().for_each(|(x, c)| {
                        if let Some(col) = grid.get_mut(x) {
                            col.push(c)
                        } else {
                            let col = vec![c];
                            grid.push(col)
                        }
                    });
                    grid
                });
        let mut new_grid = Grid {
            grid: g,
            energized: Vec::new(),
            traced_beams: BTreeSet::new(),
        };
        new_grid.init_energized();
        new_grid
    }

    fn cols(&self) -> usize {
        self.grid.len()
    }

    fn rows(&self) -> usize {
        self.grid[0].len()
    }

    fn start_beam(&mut self, (x0, y0): (usize, usize), dir: &Direction) {
        self.clear();
        if !self.traverse((x0, y0), dir) {
            return;
        };
        self.trace_beam((x0, y0), dir)
    }

    fn trace_beam(&mut self, (x0, y0): (usize, usize), dir: &Direction) {
        if !self.traced_beams.insert(((x0, y0), dir.clone())) {
            return;
        };
        let mut x = x0;
        let mut y = y0;
        self.energized[x][y] = true;
        while let Some((next_x, next_y)) = self.step((x, y), dir) {
            x = next_x;
            y = next_y;
            if !self.traverse((x, y), dir) {
                break;
            };
        }
    }

    fn traverse(&mut self, (x, y): (usize, usize), dir: &Direction) -> bool {
        self.energized[x][y] = true;
        let c = self.grid[x][y];
        if is_mirror(c) {
            self.trace_beam((x, y), &dir.reflect(c));
            return false;
        }
        if dir.split_by(c) {
            self.trace_beam((x, y), &dir.left());
            self.trace_beam((x, y), &dir.right());
            return false;
        }
        true
    }

    fn step(&self, (x, y): (usize, usize), dir: &Direction) -> Option<(usize, usize)> {
        use Direction::*;
        match dir {
            North => {
                if y > 0 {
                    return Some((x, y - 1));
                }
            }
            East => {
                if x < self.cols() - 1 {
                    return Some((x + 1, y));
                }
            }
            South => {
                if y < self.rows() - 1 {
                    return Some((x, y + 1));
                }
            }
            West => {
                if x > 0 {
                    return Some((x - 1, y));
                }
            }
        }
        return None;
    }

    fn count_energized(&self) -> usize {
        self.energized
            .iter()
            .map(|col| col.iter().filter(|e| **e).count())
            .sum()
    }

    fn init_energized(&mut self) {
        self.energized = vec![vec![false; self.rows()]; self.cols()];
    }

    fn clear(&mut self) {
        self.init_energized();
        self.traced_beams.clear();
    }

    fn find_max_energized(&mut self) -> usize {
        let mut max = 0;
        use Direction::*;
        for x in 0..self.cols() {
            self.start_beam((x, 0), &South);
            max = cmp::max(max, self.count_energized());

            self.start_beam((x, self.rows() - 1), &North);
            max = cmp::max(max, self.count_energized());
        }
        for y in 0..self.rows() {
            self.start_beam((0, y), &East);
            max = cmp::max(max, self.count_energized());

            self.start_beam((0, self.cols() - 1), &West);
            max = cmp::max(max, self.count_energized());
        }
        max
    }
}

impl fmt::Debug for Grid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for y in 0..self.grid.len() {
            for x in 0..self.grid[0].len() {
                write!(f, "{}", self.grid[x][y]).unwrap();
            }
            write!(f, "\n").unwrap();
        }
        write!(f, "\n").unwrap();

        for y in 0..self.energized.len() {
            for x in 0..self.energized[0].len() {
                if self.energized[x][y] {
                    write!(f, "#",).unwrap();
                } else {
                    write!(f, ".",).unwrap();
                }
            }
            write!(f, "\n").unwrap();
        }
        write!(f, "\n")
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn left(&self) -> Direction {
        use Direction::*;
        match self {
            North => West,
            East => North,
            South => East,
            West => South,
        }
    }

    fn right(&self) -> Direction {
        use Direction::*;
        match self {
            North => East,
            East => South,
            South => West,
            West => North,
        }
    }

    fn reflect(&self, c: char) -> Direction {
        use Direction::*;
        match self {
            North | South => match c {
                '/' => self.right(),
                '\\' | _ => self.left(),
            },
            East | West => match c {
                '/' => self.left(),
                '\\' | _ => self.right(),
            },
        }
    }

    fn split_by(&self, c: char) -> bool {
        use Direction::*;
        match self {
            North | South => c == '-',
            East | West => c == '|',
        }
    }
}

fn is_mirror(c: char) -> bool {
    c == '/' || c == '\\'
}
