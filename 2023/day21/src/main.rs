use std::{collections::BTreeSet, fmt};

fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let mut garden = Grid::parse(input);
    let locs = garden.find_reachable_gardens(64);
    // println!("{:?}", locs);
    println!("Part 1 - {}", locs.len());

    let steps = 26501365;
    let n = (steps - 66) / 131; // 66 steps to start of next pattern on edge
    let r = (steps - 66) % 131; // next repeated pattern in 131 steps
    println!("n {} r {}", n, r);

    let middle = (65, 65);
    let north = (65, 0);
    let east = (130, 65);
    let south = (65, 130);
    let west = (0, 65);
    let northeast = (130, 0);
    let southeast = (130, 130);
    let southwest = (0, 130);
    let northwest = (0, 0);

    // interior points
    let on_parity = steps % 2;
    let parity_steps = if on_parity == 0 { 201 } else { 200 };
    let on_count = garden
        .reachable_gardens(parity_steps, BTreeSet::from([middle]))
        .len() as u64;
    println!(
        "reachable from {:?} with {} steps: {} (interior {})",
        middle,
        parity_steps,
        on_count,
        if on_parity == 0 { "even" } else { "odd" }
    );
    let off_count = garden
        .reachable_gardens(parity_steps + 1, BTreeSet::from([middle]))
        .len() as u64;
    println!(
        "reachable from {:?} with {} steps: {}",
        middle,
        parity_steps + 1,
        off_count
    );

    // edge remainders
    let remainder_north = garden.reachable_gardens(r, BTreeSet::from([north])).len() as u64;
    println!(
        "reachable from {:?} with {} steps: {}",
        north, r, remainder_north
    );
    let remainder_east = garden.reachable_gardens(r, BTreeSet::from([east])).len() as u64;
    println!(
        "reachable from {:?} with {} steps: {}",
        east, r, remainder_east
    );
    let remainder_south = garden.reachable_gardens(r, BTreeSet::from([south])).len() as u64;
    println!(
        "reachable from {:?} with {} steps: {}",
        south, r, remainder_south
    );
    let remainder_west = garden.reachable_gardens(r, BTreeSet::from([west])).len() as u64;
    println!(
        "reachable from {:?} with {} steps: {}",
        west, r, remainder_west
    );

    let remainder_north_east = garden
        .reachable_gardens(r, BTreeSet::from([north, east]))
        .len() as u64;
    println!(
        "reachable from {:?} with {} steps: {}",
        [north, east],
        r,
        remainder_north_east
    );
    let remainder_south_east = garden
        .reachable_gardens(r, BTreeSet::from([east, south]))
        .len() as u64;
    println!(
        "reachable from {:?} with {} steps: {}",
        [east, south],
        r,
        remainder_south_east
    );
    let remainder_south_west = garden
        .reachable_gardens(r, BTreeSet::from([south, west]))
        .len() as u64;
    println!(
        "reachable from {:?} with {} steps: {}",
        [south, west],
        r,
        remainder_south_west
    );
    let remainder_north_west = garden
        .reachable_gardens(r, BTreeSet::from([west, north]))
        .len() as u64;
    println!(
        "reachable from {:?} with {} steps: {}",
        [west, north],
        r,
        remainder_north_west
    );

    // corner remainders
    let remainder_northeast = garden
        .reachable_gardens(r - 66, BTreeSet::from([northeast]))
        .len() as u64;
    println!(
        "reachable from {:?} with {} steps: {}",
        northeast,
        r - 66,
        remainder_northeast
    );
    let remainder_southeast = garden
        .reachable_gardens(r - 66, BTreeSet::from([southeast]))
        .len() as u64;
    println!(
        "reachable from {:?} with {} steps: {}",
        southeast,
        r - 66,
        remainder_southeast
    );
    let remainder_southwest = garden
        .reachable_gardens(r - 66, BTreeSet::from([southwest]))
        .len() as u64;
    println!(
        "reachable from {:?} with {} steps: {}",
        southwest,
        r - 66,
        remainder_southwest
    );
    let remainder_northwest = garden
        .reachable_gardens(r - 66, BTreeSet::from([northwest]))
        .len() as u64;
    println!(
        "reachable from {:?} with {} steps: {}",
        northwest,
        r - 66,
        remainder_northwest
    );

    let interior_on_sum = (n * n + 2 * n + 1) * on_count;
    let interior_off_sum = (n * n) * off_count;
    let total = interior_on_sum
        + interior_off_sum
        + remainder_north
        + remainder_east
        + remainder_south
        + remainder_west
        + n * (remainder_north_east
            + remainder_south_east
            + remainder_south_west
            + remainder_north_west)
        + (n + 1)
            * (remainder_northeast
                + remainder_southeast
                + remainder_southwest
                + remainder_northwest);

    // 608597755437807 too low
    println!("Part 2 - {}", total);
}

// input has open paths straight north/east/south/west from S
// fastest path is straight line
// 8 unreachable .'s
// different number of reachable locations if even or odd steps remaining
// r from edge mids
// r - 66 from corners
// middle even = edge odd
// middle odd = edge even

struct Grid {
    grid: Vec<Vec<char>>,
}

impl Grid {
    fn parse(input: &str) -> Grid {
        let g: Vec<Vec<char>> =
            input
                .lines()
                .map(|line| line.chars())
                .fold(Vec::new(), |mut grid, line_iter| {
                    line_iter.enumerate().for_each(|(x, c)| {
                        if let Some(col) = grid.get_mut(x) {
                            col.push(c);
                        } else {
                            let col = vec![c];
                            grid.push(col)
                        }
                    });
                    grid
                });
        let new = Grid { grid: g };
        new
    }

    fn cols(&self) -> usize {
        self.grid.len()
    }

    fn max_x(&self) -> usize {
        self.cols() - 1
    }

    fn rows(&self) -> usize {
        self.grid[0].len()
    }

    fn max_y(&self) -> usize {
        self.rows() - 1
    }

    fn step(&self, (x, y): (usize, usize), dir: &Direction) -> Option<(usize, usize)> {
        use Direction::*;
        let mut next: Option<(usize, usize)> = None;
        match dir {
            North => {
                if y > 0 {
                    next = Some((x, y - 1));
                }
            }
            East => {
                if x < self.max_x() {
                    next = Some((x + 1, y));
                }
            }
            South => {
                if y < self.max_y() {
                    next = Some((x, y + 1));
                }
            }
            West => {
                if x > 0 {
                    next = Some((x - 1, y));
                }
            }
        }
        if let Some((x1, y1)) = next {
            if self.grid[x1][y1] != '#' {
                return Some((x1, y1));
            }
        }
        return None;
    }

    fn find_reachable_gardens(&mut self, steps: u64) -> BTreeSet<(usize, usize)> {
        let mut loc = (0, 0);
        for x in 0..self.cols() {
            for y in 0..self.rows() {
                if self.grid[x][y] == 'S' {
                    loc = (x, y);
                }
            }
        }

        return self.reachable_gardens(steps, BTreeSet::from([loc]));
    }

    fn reachable_gardens(
        &mut self,
        steps: u64,
        locs: BTreeSet<(usize, usize)>,
    ) -> BTreeSet<(usize, usize)> {
        let next_locs = locs
            .into_iter()
            .map(|loc| {
                use Direction::*;
                [North, East, South, West]
                    .map(|dir| self.step(loc, &dir))
                    .into_iter()
                    .filter(|opt| *opt != None)
                    .map(|opt| opt.unwrap())
                    .fold(BTreeSet::new(), |mut a, b| {
                        a.insert(b);
                        a
                    })
            })
            .reduce(|mut a, b| {
                a.extend(&b);
                a
            })
            .unwrap();
        if steps == 1 {
            return next_locs;
        }
        self.reachable_gardens(steps - 1, next_locs)
    }
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for y in 0..self.rows() {
            for x in 0..self.cols() {
                write!(f, "{}", self.grid[x][y]).ok();
            }
            write!(f, "\n").ok();
        }
        write!(f, "\n")
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
enum Direction {
    North,
    East,
    South,
    West,
}
