use std::collections::BTreeMap;

fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params2.txt");

    let mut count = 0;
    let sum: usize = input
        .lines()
        .map(|line| {
            let mut parts = line.split_whitespace();
            let mut conditions: Vec<char> = parts.next().unwrap().chars().collect();
            count += 1;
            // print!("{}: ", count);
            // println!("{}", conditions);
            let groups: Vec<usize> = parts
                .next()
                .unwrap()
                .split(",")
                .map(|num| num.parse().unwrap())
                .collect();
            // println!("groups: {:?}", groups);

            let mut answers: BTreeMap<String, usize> = BTreeMap::new();
            let p_count = possibilities(&mut conditions, &groups, 0, &mut answers);
            // println!("{} - possibilities: {}", count, p_count);
            p_count
        })
        .sum();
    // println!("placements 3 extras into 3 spaces: {:?}", possible_placements(3, 3));


    println!("Part 1 - {}", sum);
}

fn possibilities(condition: &mut Vec<char>, groups: &Vec<usize>, initial_damaged_count: usize, answers: &mut BTreeMap<String, usize>) -> usize {
    // println!("{}  groups: {:?}, start: {}", condition.iter().collect::<String>(), groups, start);
    let key = hash_params(condition, groups, initial_damaged_count);
    if let Some(answer) = answers.get(&key) {
        return *answer;
    }
    let mut current_group;
    let mut group_index = 0;
    if group_index < groups.len() {
        current_group = groups[group_index];
    } else {
        current_group = 0;
    }
    let mut damaged_count = initial_damaged_count;
    for i in 0..condition.len() {
        match condition[i] {
            '#' => {
                if current_group == 0 {
                    // println!("{} failed 1", i);
                    answers.insert(key, 0);
                    return 0;
                }
                damaged_count += 1;
                if damaged_count > current_group {
                    // println!("{} failed 2", i);
                    answers.insert(key, 0);
                    return 0;
                }
            }
            '.' => {
                if damaged_count != 0 {
                    if damaged_count == current_group {
                        group_index += 1;
                        if group_index < groups.len() {
                            current_group = groups[group_index];
                        } else {
                            current_group = 0;
                        }
                        damaged_count = 0;
                    } else {
                        // println!("{} failed 3", i);
                        answers.insert(key, 0);
                        return 0;
                    }
                }
            }
            '?' => {
                let mut possibility = condition[i..].to_vec();
                let next_groups = groups[group_index..].to_vec();
                
                possibility[0] = '.';
                let count = possibilities(&mut possibility, &next_groups, damaged_count, answers);

                possibility[0] = '#';
                let count2 = possibilities(&mut possibility, &next_groups, damaged_count, answers); 
                if count + count2 == 0 {
                    // println!("{} failed 4", i);
                }

                answers.insert(key, count + count2);
                return count + count2;
            }
            _ => {
                answers.insert(key, 0);
                return 0;
            }
        }
    }
    if group_index == groups.len() {
        // println!("passed");
        answers.insert(key, 1);
        return 1;
    }
    if group_index == groups.len() - 1 && damaged_count == current_group {
        // println!("passed");
        answers.insert(key, 1);
        return 1;
    }
    // println!("failed 5 groups: {:?} group_index: {}, damaged_count: {}, current_group {}", groups, group_index, damaged_count, current_group);
    answers.insert(key, 0);
    return 0;
}

fn hash_params(condition: &mut Vec<char>, groups: &Vec<usize>, initial_damaged_count: usize) -> String {
    let c = condition.iter().collect::<String>();
    let g_opt = groups.iter().map(|g| g.to_string()).reduce(|a, b| {
        a + &b
    });
    let g = match g_opt {
        Some(val) => val,
        None => "[]".to_owned()
    };
    let d = initial_damaged_count.to_string();
    c + " " + &g + " " + &d
}