use std::{collections::{BTreeMap, VecDeque}, cmp};
use num::integer::lcm;


fn main() {
    use Module::*;

    let input = include_str!("../example.txt");
    let input = include_str!("../example2.txt");
    let input = include_str!("../params.txt");

    let mut mods: BTreeMap<String, Module> = BTreeMap::new();
    let mut broadcaster = Vec::new();
    let mut rx_source = String::new();
    input.lines().for_each(|line| {
        let mut parts = line.split(" -> ");
        let p1 = parts.next().unwrap();
        let p2 = parts.next().unwrap();
        let ds: Vec<String> = p2.split(", ").map(|s| s.to_string()).collect();
        let name = p1[1..].to_string();
        if ds.contains(&"rx".to_string()) {
            rx_source = name.clone();
        }
        match &p1[..1] {
            "%" => {
                mods.insert(
                    name,
                    FlipFlop {
                        on: false,
                        destinations: ds,
                    },
                );
            }
            "&" => {
                mods.insert(
                    name,
                    Conjunction {
                        source_states: BTreeMap::new(),
                        destinations: ds,
                    },
                );
            }
            "b" => {
                broadcaster = ds;
            }
            _ => {}
        }
    });
    let mut rx_source_sources: Vec<String> = Vec::new();
    input.lines().for_each(|line| {
        let mut parts = line.split(" -> ");
        let p1 = parts.next().unwrap();
        let p2 = parts.next().unwrap();
        let ds = p2.split(", ").map(|s| s.to_string());
        let name = p1[1..].to_string();
        for d in ds {
            if let Some(m) = mods.get_mut(&d) {
                match m {
                    Conjunction {
                        source_states,
                        destinations: _,
                    } => {
                        source_states.insert(name.clone(), false);
                    }
                    _ => {}
                }
            }
            if d == rx_source {
                rx_source_sources.push(name.clone());
            }
        }
    });

    println!("{:?}", broadcaster);
    println!("{:?}", mods);

    println!("{} {:?}", rx_source, rx_source_sources);

    let mut cycles_to_high = Vec::new();

    let mut q: VecDeque<(String, String, bool)> = VecDeque::new();
    let mut button_presses: i64 = 0;
    let mut high_count: i64 = 0;
    let mut low_count: i64 = 0;
    let mut rx_count = 0;
    loop {
        // push button
        if button_presses % 1000 == 0 {
            println!("button presses: {} low: {} high: {}", button_presses, low_count, high_count);
        }
        button_presses += 1;
        if button_presses <= 1000 {
            low_count += 1;
        }
        for dest in &broadcaster {
            // println!("broadcaster -false-> {}", dest);
            q.push_back(("broadcaster".to_string(), dest.clone(), false));
        }
        while let Some((source, dest, high)) = q.pop_front() {
            if button_presses <= 1000 {
                if high {
                    high_count += 1;
                } else {
                    low_count += 1;
                }
            }
            if !high && rx_count == 0 && dest == "rx" {
                rx_count = button_presses;
            }
            // println!("dest \"{}\"", dest);
            if let Some(m) = mods.get_mut(&dest) {
                match m {
                    FlipFlop { on, destinations } => {
                        if !high {
                            *on = !*on;
                            for d in destinations {
                                // println!("{} -{}-> {}", dest, on, d);
                                q.push_back((dest.clone(), d.clone(), *on));
                            }
                        }
                    }
                    Conjunction {
                        source_states,
                        destinations,
                    } => {
                        source_states.insert(source, high);
                        let all_high = source_states.iter().all(|(_, high)| *high);
                        for d in destinations {
                            // println!("{} -{}-> {}", dest, !all_high, d);
                            if !all_high {
                                if rx_source_sources.contains(&dest) {
                                   println!("&{} high at {}", dest, button_presses);
                                   cycles_to_high.push(button_presses);
                                }
                            }
                            q.push_back((dest.clone(), d.clone(), !all_high));
                        }
                    }
                }
            }
        }
        if mods.values().all(|val| {
            match val {
                FlipFlop {
                    on,
                    destinations: _,
                } => !*on,
                Conjunction {
                    source_states,
                    destinations: _,
                } => {
                    source_states.values().all(|high| !*high)
                }
            }
        }) || cycles_to_high.len() == rx_source_sources.len() {
            break;
        }
    }
    // println!("{:?}", mods);
    println!(
        "low {} high {} presses {}",
        low_count, high_count, button_presses
    );
    println!(
        "Part 1 - {}",
        low_count * high_count * 1000 / cmp::min(button_presses, 1000)
    );

    println!("Part 2 - {}", cycles_to_high.into_iter().reduce(|a, b| lcm(a, b)).unwrap());
}

#[derive(Debug)]
enum Module {
    FlipFlop {
        on: bool,
        destinations: Vec<String>,
    },
    Conjunction {
        source_states: BTreeMap<String, bool>,
        destinations: Vec<String>,
    },
}
