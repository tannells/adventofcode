use std::cmp;

fn main() {
    // let input = include_str!("../example.txt");
    // let input = include_str!("../example2.txt");
    let input = include_str!("../params.txt");
    let parts: Vec<&str> = input.split("\n\n").collect();
    let patterns: Vec<Vec<Vec<char>>> = parts
        .iter()
        .map(|part| part.lines().map(|line| line.chars().collect()).collect())
        .collect();

    let sum: usize = patterns
        .iter()
        .map(|pattern| match find_mirror(pattern, false) {
            Orientation::Vertical(x) => x,
            Orientation::Horizontal(y) => 100 * y,
        })
        .sum();

    println!("Part 1 - {}", sum);

    let sum: usize = patterns
        .iter()
        .map(|pattern| match find_mirror(pattern, true) {
            Orientation::Vertical(x) => x,
            Orientation::Horizontal(y) => 100 * y,
        })
        .sum();

    println!("Part 2 - {}", sum);
}

fn find_mirror(pattern: &Vec<Vec<char>>, smudged: bool) -> Orientation {
    let rows = pattern.len();
    let cols = pattern[0].len();
    let target_mismatches = if smudged { 2 } else { 1 };

    let mut mismatch;
    for x in 1..(cols) {
        mismatch = 0;
        let steps = cmp::min(x, cols - x);

        for s in 1..=steps {
            for y in 0..rows {
                if pattern[y][x - s] != pattern[y][x + s - 1] {
                    mismatch += 1;
                }
            }
        }
        if mismatch == target_mismatches - 1 {
            return Orientation::Vertical(x);
        }
    }
    for y in 1..(rows) {
        mismatch = 0;
        let steps = cmp::min(y, rows - y);

        for s in 1..=steps {
            for x in 0..cols {
                if pattern[y - s][x] != pattern[y + s - 1][x] {
                    mismatch += 1;
                }
            }
        }
        if mismatch == target_mismatches - 1 {
            return Orientation::Horizontal(y);
        }
    }
    return Orientation::Horizontal(0);
}

enum Orientation {
    Vertical(usize),
    Horizontal(usize),
}
