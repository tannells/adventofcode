fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let mut map: Vec<Vec<char>> = Vec::new();

    for line in input.lines() {
        map.push(line.chars().collect())
    }
    let mut part_numbers: Vec<usize> = Vec::new();
    let mut current_number = String::new();
    let mut starting_x;
    let max_x = map.first().unwrap().len() - 1;
    let max_y = map.len() - 1;
    for y in 0..=max_y {
        starting_x = 0;
        for x in 0..=max_x {
            if map[y][x].is_ascii_digit() {
                current_number.push(map[y][x]);
                if x == max_x {
                    if is_part_number(&map, starting_x, x, y) {
                        part_numbers.push(current_number.parse::<usize>().unwrap());
                    }
                    current_number.clear();
                }
            } else if !current_number.is_empty() {
                if is_part_number(&map, starting_x, x, y) {
                    part_numbers.push(current_number.parse::<usize>().unwrap());
                }
                current_number.clear();
                starting_x = x
            } else {
                starting_x = x
            }
        }
    }

    let sum: usize = part_numbers.iter().sum();
    println!("Part 1 - {}", sum);

    let mut gear_ratio_sum = 0;
    for y in 0..=max_y {
        for x in 0..=max_x {
            gear_ratio_sum += gear_ratio(&map, x, y)
        }
    }
    println!("Part 2 - {}", gear_ratio_sum);
}

fn is_part_number(map: &Vec<Vec<char>>, x_i: usize, x_f: usize, y0: usize) -> bool {
    let max_y = map.len() - 1;
    let y_i: usize;
    let y_f: usize;
    if y0 > 0 {
        y_i = y0 - 1
    } else {
        y_i = 0
    }
    if y0 < max_y {
        y_f = y0 + 1
    } else {
        y_f = max_y
    }
    let mut result = false;
    for y in y_i..=y_f {
        for x in x_i..=x_f {
            // print!{"{}", map[y][x]}
            if !map[y][x].is_ascii_digit() && map[y][x] != '.' {
                result = true
            }
        }
        // print!{"\n"}
    }
    // print!{"{}\n\n", result}
    return result;
}

fn gear_ratio(map: &Vec<Vec<char>>, x0: usize, y0: usize) -> usize {
    if map[y0][x0] != '*' {
        return 0;
    }

    let max_x = map.first().unwrap().len() - 1;
    let max_y = map.len() - 1;
    let x_i: usize;
    let x_f: usize;
    let y_i: usize;
    let y_f: usize;
    if x0 > 0 {
        x_i = x0 - 1
    } else {
        x_i = 0
    }
    if x0 < max_x {
        x_f = x0 + 1
    } else {
        x_f = max_x
    }
    if y0 > 0 {
        y_i = y0 - 1
    } else {
        y_i = 0
    }
    if y0 < max_y {
        y_f = y0 + 1
    } else {
        y_f = max_y
    }
    let mut same_number = false;
    let mut part_count = 0;
    let mut adjacent_part_numbers = Vec::new();
    for y in y_i..=y_f {
        for x in x_i..=x_f {
            // print!("{}", map[y][x]);
            if map[y][x].is_ascii_digit() {
                if !same_number {
                    adjacent_part_numbers.push(full_number(map, x, y));
                    part_count = part_count + 1
                }
                same_number = true
            } else {
                same_number = false
            }
        }
        // print!("\n");
        same_number = false
    }
    // print!("{:?}\n\n", adjacent_part_numbers);
    if adjacent_part_numbers.len() == 2 {
        return adjacent_part_numbers.iter().product();
    } else {
        return 0;
    }
}

fn full_number(map: &Vec<Vec<char>>, x0: usize, y0: usize) -> usize {
    let max_x = map.first().unwrap().len() - 1;
    let mut x_i = x0;
    // go left until we find the start of the number
    for x in (0..x0).rev() {
        if !map[y0][x].is_ascii_digit() {
            break;
        }
        x_i = x
    }
    // add characters to the right until the end of the number
    let mut num_str = String::new();
    for x in x_i..=max_x {
        if !map[y0][x].is_ascii_digit() {
            break;
        }
        num_str.push(map[y0][x])
    }
    return num_str.parse::<usize>().unwrap();
}
