use std::collections::BTreeMap;

use lazy_regex::regex_captures;

fn main() {
    let input = include_str!("../example.txt");
    // let input = include_str!("../params.txt");

    let mut parts = input.split("\n\n");
    let mut workflows: BTreeMap<String, Workflow> = BTreeMap::new();
    parts.next().unwrap().lines().for_each(|l| {
        let (_, name, rules_str, default) = regex_captures!(r#"(\w*)\{(.*),(\w+)\}"#, l).unwrap();
        workflows.insert(name.to_string(), Workflow {
            name: name.to_string(),
            rules: ,
            default_destination: default.to_string(),
        });
    });

    // println!("Part 1 - {}", );

    // println!("Part 2 - {}", );
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
struct Rule {
    category: char,
    op: char,
    rating: u32,
    destination: String
}

impl Rule {
    fn from(string: &str) -> Vec<Rule> {
        let mut rules = Vec::new();
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
struct Workflow {
    name: String,
    rules: Vec<Rule>,
    default_destination: String,
}