fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let points = input.lines().fold(0, |points, card| {
        let wins = count_wins(card);
        if wins > 0 {
            points + usize::pow(2, (wins - 1).try_into().unwrap())
        } else {
            points
        }
    });

    println!("Part 1 - {}", points);

    let cards: Vec<&str> = input.lines().collect();
    let mut card_counts: Vec<usize> = vec![1; input.lines().count()];
    for i in 0..card_counts.len() {
        let wins = count_wins(cards[i]);
        for j in i + 1..=i + wins {
            card_counts[j] += card_counts[i]
        }
    }
    let sum = card_counts.iter().sum::<usize>();
    println!("Part 2 - {}", sum);
}

fn count_wins(card: &str) -> usize {
    let mut numbers = card.split(":").last().unwrap().split("|");
    let winning_numbers: Vec<usize> = numbers
        .next()
        .unwrap()
        .split_whitespace()
        .map(|num| num.parse::<usize>().unwrap())
        .collect();
    let my_numbers: Vec<usize> = numbers
        .next()
        .unwrap()
        .split_whitespace()
        .map(|num| num.parse::<usize>().unwrap())
        .collect();
    let matches = winning_numbers.iter().fold(0, |matches, win_num| {
        if my_numbers.iter().any(|my_num| win_num == my_num) {
            matches + 1
        } else {
            matches
        }
    });
    return matches;
}
