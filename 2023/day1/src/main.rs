use lazy_regex::{regex_captures, regex_replace_all};

fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let sum = input.lines().fold(0, |sum, line| {
        let (_whole, first) = regex_captures!(r#"(\d).*"#, line).unwrap();
        let (_whole, last) = regex_captures!(r#".*(\d)"#, line).unwrap();
        let mut both: String = first.to_string();
        both.push_str(last);
        let value = both.parse::<usize>().unwrap();
        // println!("{}", value);
        sum + value
    });

    println!("Part 1 - {}", sum);

    // let input = include_str!("../example2.txt");

    let sum2 = input.lines().fold(0, |sum, line| {
        let (_whole, first) = regex_captures!(
            r#"(\d|one|two|three|four|five|six|seven|eight|nine).*"#,
            line
        )
        .unwrap();
        let (_whole, last) = regex_captures!(
            r#".*(\d|one|two|three|four|five|six|seven|eight|nine)"#,
            line
        )
        .unwrap();
        let mut both: String = convert(first);
        both.push_str(&convert(last));
        let value = both.parse::<usize>().unwrap();
        // println!("{}", value);
        sum + value
    });
    println!("Part 2 - {}", sum2);
}

fn convert(val: &str) -> String {
    return regex_replace_all!(
        r#"(one|two|three|four|five|six|seven|eight|nine)"#,
        val,
        |number, _| match number {
            "one" => Some("1"),
            "two" => Some("2"),
            "three" => Some("3"),
            "four" => Some("4"),
            "five" => Some("5"),
            "six" => Some("6"),
            "seven" => Some("7"),
            "eight" => Some("8"),
            "nine" => Some("9"),
            _ => None,
        }
        .unwrap(),
    )
    .to_string();
}
