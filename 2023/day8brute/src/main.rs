use lazy_regex::regex_captures;
use std::collections::BTreeMap;

fn main() {
    let input = include_str!("../example.txt");
    let input = include_str!("../example2.txt");
    // let input = include_str!("../example3.txt");
    let input = include_str!("../params.txt");

    let mut parts = input.split("\n\n");
    let instructions: Vec<char> = parts.next().unwrap().chars().collect();
    let mut network: BTreeMap<&str, (&str, &str)> = BTreeMap::new();
    for line in parts.next().unwrap().lines() {
        let (_, node, left, right) =
            regex_captures!(r#"(\w{3}) = \((\w{3}), (\w{3})\)"#, line).unwrap();
        network.insert(node, (left, right));
    }

    // let mut loc = "AAA";
    // let mut count = 0;
    // while loc != "ZZZ" {
    //     let dir = instructions[count % instructions.len()];
    //     let next_locs = network.get(loc).unwrap();
    //     if dir == 'L' {
    //         loc = next_locs.0
    //     } else {
    //         loc = next_locs.1
    //     }
    //     count += 1;
    // }
    //
    // println!("Part 1 - {}", count);

    let mut locs: Vec<&str> = network
        .keys()
        .filter(|node| node.chars().last().unwrap() == 'A')
        .copied()
        .collect();
    let mut count = 0;
    while locs.iter().any(|loc| loc.chars().last().unwrap() != 'Z') {
        for i in 0..locs.len() {
            let dir = instructions[count % instructions.len()];
            let next_locs = network.get(&locs[i]).unwrap();
            if dir == 'L' {
                locs[i] = next_locs.0
            } else {
                locs[i] = next_locs.1
            }
        }
        count += 1;
    }

    println!("Part 2 - {}", count);
}
