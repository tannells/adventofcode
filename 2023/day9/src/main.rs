fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");
    let histories: Vec<Vec<i32>> = parse(input);

    let sum1: i32 = extrapolations(&histories, false).iter().sum();
    println!("Part 1 - {}", sum1);

    let sum2: i32 = extrapolations(&histories, true).iter().sum();
    println!("Part 2 - {}", sum2);
}

fn parse(input: &str) -> Vec<Vec<i32>> {
    input
        .lines()
        .map(|line| {
            line.split_whitespace()
                .map(|val| val.parse().unwrap())
                .collect()
        })
        .collect()
}

fn extrapolate(history: &Vec<i32>, backwards: bool) -> i32 {
    let mut current = history.clone();
    if backwards {
        current.reverse()
    }
    let mut sum = 0;
    while current.iter().any(|x| *x != 0) {
        for i in 0..current.len()-1 {
            current[i] = current[i + 1] - current[i]
        }
        sum += current.pop().unwrap();
    }
    sum
}

fn extrapolations(histories: &Vec<Vec<i32>>, backwards: bool) -> Vec<i32> {
    histories
        .iter()
        .map(|history| extrapolate(&history, backwards))
        .collect()
}
