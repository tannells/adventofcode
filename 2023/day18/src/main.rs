use lazy_regex::regex_captures;

fn main() {
    let input = include_str!("../example.txt");

    let mut points1: Vec<(i64, i64)> = vec![(0, 0)];
    let mut points2: Vec<(i64, i64)> = vec![(0, 0)];

    input.lines().for_each(|line| {
        let (_, direction, distance, hex_color) =
            regex_captures!(r#"(U|R|D|L) (\d+) \(#(.*)\)"#, line).unwrap();
        let d = distance.parse::<i64>().unwrap();
        let loc = points1.last().unwrap();
        let next = step(*loc, direction, d);
        points1.push(next);

        let d2 = i64::from_str_radix(&hex_color[0..=4], 16).unwrap();
        let dir2 = match &hex_color[5..] {
            "0" => "R",
            "1" => "D",
            "2" => "L",
            "3" => "U",
            _ => "",
        };
        let loc2 = points2.last().unwrap();
        let next2 = step(*loc2, dir2, d2);
        points2.push(next2);
    });

    println!("Part 1 - {}", area(&points1));

    println!("Part 2 - {}", area(&points2));
}

fn step((x, y): (i64, i64), direction: &str, d: i64) -> (i64, i64) {
    match direction {
        "U" => (x, y - d),
        "R" => (x + d, y),
        "D" => (x, y + d),
        "L" => (x - d, y),
        _ => (x, y),
    }
}

fn area(points: &Vec<(i64, i64)>) -> i64 {
    let (mut x0, mut y0) = (0, 0);
    points.iter().fold(0, |area, (x1, y1)| {
        let a = x0 * y1 - x1 * y0 + (x1 - x0).abs() + (y1 - y0).abs();
        x0 = *x1;
        y0 = *y1;
        area + a
    }) / 2
        + 1
}
