use std::{
    cmp,
    collections::{BTreeMap, VecDeque},
    fmt,
    time::Instant,
};

fn main() {
    // let input = include_str!("../example.txt"); // 102, 94
    let input = include_str!("../params.txt");

    let mut city = Grid::parse(input);
    let time = Instant::now();
    let min = city.find_min_heat_loss(false);
    println!("Part 1 - {}, {:.2?}", min, time.elapsed());

    let time = Instant::now();
    let min2 = city.find_min_heat_loss(true);
    println!("Part 2 - {}, {:.2?}", min2, time.elapsed());
}

struct Grid {
    grid: Vec<Vec<u32>>,
    mins: Vec<Vec<BTreeMap<Direction, BTreeMap<u8, u32>>>>,
    paths: VecDeque<Path>,
    min: u32,
}

impl Grid {
    fn parse(input: &str) -> Grid {
        let g: Vec<Vec<u32>> =
            input
                .lines()
                .map(|line| line.chars())
                .fold(Vec::new(), |mut grid, line_iter| {
                    line_iter.enumerate().for_each(|(x, c)| {
                        if let Some(col) = grid.get_mut(x) {
                            col.push(c.to_digit(10).unwrap());
                        } else {
                            let col = vec![c.to_digit(10).unwrap()];
                            grid.push(col)
                        }
                    });
                    grid
                });
        let mut new = Grid {
            mins: Vec::new(),
            grid: g,
            paths: VecDeque::new(),
            min: u32::MAX,
        };
        new.initialize_mins();
        new
    }

    fn initialize_mins(&mut self) {
        self.mins = vec![vec![BTreeMap::new(); self.rows()]; self.cols()];
        self.min = u32::MAX;
    }

    fn cols(&self) -> usize {
        self.grid.len()
    }

    fn max_x(&self) -> usize {
        self.cols() - 1
    }

    fn rows(&self) -> usize {
        self.grid[0].len()
    }

    fn max_y(&self) -> usize {
        self.rows() - 1
    }

    fn step(
        &self,
        (x, y): (usize, usize),
        dir: &Direction,
        step_size: usize,
    ) -> Option<(usize, usize)> {
        use Direction::*;
        match dir {
            North => {
                if y >= step_size {
                    return Some((x, y - step_size));
                }
            }
            East => {
                if x < self.cols() - step_size {
                    return Some((x + step_size, y));
                }
            }
            South => {
                if y < self.rows() - step_size {
                    return Some((x, y + step_size));
                }
            }
            West => {
                if x >= step_size {
                    return Some((x - step_size, y));
                }
            }
        }
        return None;
    }

    fn find_min_heat_loss(&mut self, ultra: bool) -> u32 {
        self.initialize_mins();
        let starting_path = Path {
            location: (0, 0),
            heat_loss: 0,
            direction: Direction::East,
            straight_count: 0,
        };
        self.paths.push_back(starting_path);
        while let Some(path) = self.paths.pop_front() {
            for dir in path.next_steps(ultra) {
                self.advance_path(&path, dir, ultra)
            }
        }
        self.min
    }

    fn advance_path(&mut self, path: &Path, dir: Direction, ultra: bool) {
        let mut p = path.clone();
        let loc = path.location;
        if ultra && p.direction != dir && self.step(loc, &dir, 4).is_none() {
            // ultra crucible needs to be able to move 4 in a direction before stopping
            return;
        }
        if let Some((x, y)) = self.step(loc, &dir, 1) {
            p.location = (x, y);
            if p.direction != dir {
                p.direction = dir;
                p.straight_count = 0;
            }
            p.straight_count += 1;
            p.heat_loss += self.grid[x][y];
            if (x, y) == (self.max_x(), self.max_y()) {
                self.min = cmp::min(self.min, p.heat_loss);
            }

            if p.heat_loss > self.min {
                return;
            }

            let dir_map = &mut self.mins[x][y];
            if let Some(count_map) = dir_map.get_mut(&p.direction) {
                if let Some(heat_loss) = count_map.get_mut(&p.straight_count) {
                    // found way to same spot with lower heat loss
                    if p.heat_loss < *heat_loss {
                        *heat_loss = p.heat_loss;
                        self.paths.push_back(p);
                    }
                } else {
                    if ultra
                        || count_map.keys().any(|c| p.straight_count < *c)
                        || count_map.values().any(|h| p.heat_loss < *h)
                    {
                        // if ultra, or
                        // if we got here with fewer straights in a row, or
                        // if we got here with more straights in a row, but lower heat loss
                        count_map.insert(p.straight_count, p.heat_loss);
                        self.paths.push_back(p);
                    }
                }
            } else {
                // first time to this spot
                let mut count_map = BTreeMap::new();
                count_map.insert(p.straight_count, p.heat_loss);
                dir_map.insert(p.direction.clone(), count_map);
                self.paths.push_back(p);
            }
        }
    }
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for y in 0..self.rows() {
            for x in 0..self.cols() {
                write!(f, "{}", self.grid[x][y]).ok();
            }
            write!(f, "\n").ok();
        }
        write!(f, "\n").ok();

        for y in 0..self.rows() {
            for x in 0..self.cols() {
                let mins = &self.mins[x][y];
                write!(f, "{:?}", mins).ok();
            }
            write!(f, "\n").ok();
        }
        write!(f, "\n")
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
enum Direction {
    North,
    East,
    South,
    West,
}

impl Direction {
    fn left(&self) -> Direction {
        use Direction::*;
        match self {
            North => West,
            East => North,
            South => East,
            West => South,
        }
    }

    fn right(&self) -> Direction {
        use Direction::*;
        match self {
            North => East,
            East => South,
            South => West,
            West => North,
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
struct Path {
    location: (usize, usize),
    heat_loss: u32,
    direction: Direction,
    straight_count: u8,
}

impl Path {
    fn next_steps(&self, ultra: bool) -> Vec<Direction> {
        let mut next = Vec::new();
        if ultra {
            if self.straight_count < 10 {
                next.push(self.direction.clone())
            }
            if self.straight_count >= 4 {
                next.push(self.direction.left());
                next.push(self.direction.right());
            }
        } else {
            if self.straight_count < 3 {
                next.push(self.direction.clone());
            }
            next.push(self.direction.left());
            next.push(self.direction.right());
        }
        next
    }
}
