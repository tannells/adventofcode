use std::collections::BTreeMap;

fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let mut platform = Grid::parse(input);

    platform.slide(&Direction::North);
    println!("Part 1 - {}", platform.total_load(&Direction::North));

    platform.cycle_until_repeat(1000);

    platform.state_at(1000000000);

    println!("Part 2 - {}", platform.total_load(&Direction::North));
}

#[derive(Debug)]
struct Grid {
    rows: Vec<Vec<char>>,
    cache: BTreeMap<String, Vec<Vec<char>>>,
    repeat_start: usize,
    step: usize,
    repeat_keys: Vec<String>,
}

impl Grid {
    fn parse(input: &str) -> Grid {
        let rows: Vec<Vec<char>> = input.lines().map(|line| line.chars().collect()).collect();
        let mut cols = Vec::new();

        for _ in 0..rows.len() {
            cols.push(Vec::new())
        }
        for row in rows.iter() {
            for (index, char) in row.iter().enumerate() {
                cols[index].push(*char)
            }
        }

        Grid {
            rows: rows,
            cache: BTreeMap::new(),
            repeat_start: 0,
            step: 0,
            repeat_keys: Vec::new(),
        }
    }

    fn get_cols(&self, dir: &Direction) -> Vec<Vec<char>> {
        let mut cols = Vec::new();

        match dir {
            Direction::North => {
                for _ in 0..self.rows.len() {
                    cols.push(Vec::new())
                }
                for row in self.rows.iter() {
                    for (index, char) in row.iter().enumerate() {
                        cols[index].push(*char)
                    }
                }
            }
            Direction::East => {
                for i in 0..self.rows.len() {
                    let mut col = self.rows[i].clone();
                    col.reverse();
                    cols.push(col);
                }
            }
            Direction::South => {
                for _ in 0..self.rows.len() {
                    cols.push(Vec::new())
                }
                for row in self.rows.iter().rev() {
                    for (index, char) in row.iter().enumerate() {
                        cols[index].push(*char)
                    }
                }
            }
            Direction::West => cols = self.rows.clone(),
        }
        cols
    }

    fn set_cols(&mut self, mut new_cols: Vec<Vec<char>>, dir: &Direction) {
        match dir {
            Direction::North => {
                self.rows = Vec::new();
                for _ in 0..new_cols.len() {
                    self.rows.push(Vec::new())
                }
                for col in new_cols {
                    for (index, char) in col.iter().enumerate() {
                        self.rows[index].push(*char)
                    }
                }
            }
            Direction::East => {
                for col in new_cols.iter_mut() {
                    col.reverse();
                }
                self.rows = new_cols;
            }
            Direction::South => {
                self.rows = Vec::new();
                for _ in 0..new_cols.len() {
                    self.rows.push(Vec::new())
                }
                for col in new_cols.into_iter() {
                    for (index, char) in col.iter().rev().enumerate() {
                        self.rows[index].push(*char)
                    }
                }
            }
            Direction::West => self.rows = new_cols,
        };
    }

    fn total_load(&self, dir: &Direction) -> usize {
        let cols = self.get_cols(dir);

        let mut load = 0;
        for col in cols.iter() {
            for (index, char) in col.iter().enumerate() {
                match *char {
                    'O' => load += col.len() - index,
                    _ => {}
                }
            }
        }
        load
    }

    fn slide(&mut self, dir: &Direction) -> bool {
        let cols = self.get_cols(dir);
        let (new_cols, stop) = self.slide_cols(&cols);
        self.set_cols(new_cols, dir);
        stop
    }

    fn slide_cols(&mut self, cols: &Vec<Vec<char>>) -> (Vec<Vec<char>>, bool) {
        let key = Grid::cols_to_str(&cols);
        if let Some(answer) = self.cache.get(&key) {
            if self.repeat_start == 0 {
                self.repeat_start = self.step;
            }
            if self.repeat_keys.contains(&key) {
                return (answer.clone(), true);
            } else {
                self.repeat_keys.push(key);
                return (answer.clone(), false);
            }
        }
        let new_cols: Vec<Vec<char>> = cols.into_iter().map(|col| self.slide_col(&col)).collect();
        self.cache.insert(key, new_cols.clone());
        (new_cols, false)
    }

    fn slide_col(&mut self, col: &Vec<char>) -> Vec<char> {
        let mut new_col = Vec::new();
        let mut count = 0;
        for (index, char) in col.iter().enumerate() {
            match *char {
                'O' => {
                    count += 1;
                }
                '#' => {
                    for _ in 0..count {
                        new_col.push('O');
                    }
                    for _ in 0..(index - new_col.len()) {
                        new_col.push('.')
                    }
                    new_col.push('#');
                    count = 0;
                }
                _ => {}
            }
        }
        for _ in 0..count {
            new_col.push('O');
        }
        for _ in 0..(col.len() - new_col.len()) {
            new_col.push('.')
        }
        new_col
    }

    fn cols_to_str(cols: &Vec<Vec<char>>) -> String {
        let mut out = String::new();
        for col in cols {
            for char in col {
                out.push(*char)
            }
        }
        out
    }

    fn cycle_until_repeat(&mut self, limit: usize) {
        'outer: for _ in 0..limit {
            for dir in [
                Direction::North,
                Direction::West,
                Direction::South,
                Direction::East,
            ] {
                if self.slide(&dir) {
                    break 'outer;
                }
                self.step += 1;
            }
        }
    }

    fn state_at(&mut self, cycle: usize) {
        // cycles after the cycle the repeated pattern started
        let cycle_offset = (cycle - self.repeat_start / 4 - 1) % (self.repeat_keys.len() / 4);
        // cycle_offset * 4 -> steps after the first step of the cycle when the repeated pattern started
        // - self.repeat_start % 4 -> offset by how many steps into the cycle the pattern started repeating
        // + 3 -> end of that cycle
        let step_offset = cycle_offset * 4 - self.repeat_start % 4 + 3;
        let key = &self.repeat_keys[step_offset];
        let result = self.cache.get(key).unwrap().clone();
        self.set_cols(result, &Direction::East);
    }

    fn print_grid(&self) {
        for y in 0..self.rows.len() {
            for x in 0..self.rows[0].len() {
                print!("{}", self.rows[y][x])
            }
            print!("\n")
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
enum Direction {
    North,
    East,
    South,
    West,
}
