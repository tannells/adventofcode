use std::{cmp::Ordering, collections::BTreeSet, fmt, ops::RangeInclusive};

use lazy_regex::regex_captures;

fn main() {
    let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let mut s = Stack::from(input);

    // println!("{:?}", s);

    // println!("{}", s);
    s.settle();
    // println!("{:?}", s);
    // 497 too high
    // println!("{}", s);
    println!("Part 1 - {}", s.count_removable());

    println!("Part 2 - {}", s.count_chain_reaction());
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
struct Block {
    endpoints: ((usize, usize, usize), (usize, usize, usize)),
    id: usize,
}

impl Block {
    fn coordinates(&self) -> Vec<usize> {
        let mut cs = Vec::new();
        // if z's are different, sort by the higher one
        cs.push(self.endpoints.1 .2);
        cs.push(self.endpoints.0 .2);
        cs.push(self.endpoints.0 .1);
        cs.push(self.endpoints.1 .1);
        cs.push(self.endpoints.0 .0);
        cs.push(self.endpoints.1 .0);
        cs
    }

    fn x_range(&self) -> RangeInclusive<usize> {
        self.endpoints.0 .0..=self.endpoints.1 .0
    }

    fn y_range(&self) -> RangeInclusive<usize> {
        self.endpoints.0 .1..=self.endpoints.1 .1
    }

    fn z_range(&self) -> RangeInclusive<usize> {
        self.endpoints.0 .2..=self.endpoints.1 .2
    }

    fn x_y_intersect(&self, other: &Self) -> bool {
        for x in self.x_range() {
            for y in self.y_range() {
                if other.x_range().contains(&x) && other.y_range().contains(&y) {
                    return true;
                }
            }
        }
        return false;
    }

    fn is_on(&self, other: &Self) -> bool {
        other.z_range().end() + 1 == *self.z_range().start() && self.x_y_intersect(other)
    }

    fn fall_to(&mut self, z: usize) {
        let dz = self.endpoints.0 .2 - z;
        self.endpoints.0 .2 -= dz;
        self.endpoints.1 .2 -= dz;
    }
}

impl Ord for Block {
    fn cmp(&self, other: &Self) -> Ordering {
        self.coordinates().cmp(&other.coordinates())
    }
}

impl PartialOrd for Block {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.coordinates().cmp(&other.coordinates()))
    }
}

#[derive(Debug)]
struct Stack {
    blocks: BTreeSet<Block>,
    chain_removed: BTreeSet<usize>,
}

impl Stack {
    fn from(input: &str) -> Stack {
        let mut blocks: BTreeSet<Block> = BTreeSet::new();
        input.lines().enumerate().for_each(|(i, line)| {
            let (_, x0, y0, z0, x1, y1, z1) =
                regex_captures!(r#"(\d+),(\d+),(\d+)~(\d+),(\d+),(\d+)"#, line).unwrap();
            let new_block = Block {
                endpoints: (
                    (
                        x0.parse::<usize>().unwrap(),
                        y0.parse::<usize>().unwrap(),
                        z0.parse::<usize>().unwrap(),
                    ),
                    (
                        x1.parse::<usize>().unwrap(),
                        y1.parse::<usize>().unwrap(),
                        z1.parse::<usize>().unwrap(),
                    ),
                ),
                id: i,
            };
            // println!("{} <= {} {}", x0, x1, x0.parse::<usize>().unwrap() <= x1.parse::<usize>().unwrap());
            // println!("{} <= {} {}", y0, y1, y0.parse::<usize>().unwrap() <= y1.parse::<usize>().unwrap());
            // println!("{} <= {} {}", z0, z1, z0.parse::<usize>().unwrap() <= z1.parse::<usize>().unwrap());
            blocks.insert(new_block);
        });
        Stack {
            blocks: blocks,
            chain_removed: BTreeSet::new(),
        }
    }

    fn settle(&mut self) {
        let blocks: Vec<Block> = self.blocks.iter().cloned().collect();
        self.blocks = BTreeSet::new();
        blocks.into_iter().for_each(|b| {
            // println!("adding {}", i);
            self.add(b);
        })
    }

    fn add(&mut self, mut b: Block) {
        for other in self.blocks.iter().rev() {
            if b.x_y_intersect(other) {
                // println!("{:?} {:?}", b, other);
                b.fall_to(other.z_range().end() + 1);
                self.blocks.insert(b);
                return;
            }
        }
        b.fall_to(1);
        self.blocks.insert(b);
    }

    fn count_removable(&self) -> i32 {
        let mut count = 0;
        self.blocks.iter().for_each(|b| {
            let removable = self
                .blocks
                .iter()
                .filter(|supported| supported.is_on(b))
                .all(|supported|
                // all blocks supported by b are supported by more than 1
                self.blocks.iter().filter(|support| {
                    supported.is_on(support)
                }).count() > 1);
            if removable {
                // println!("{:?}", b);
                count += 1;
            }

            // find blocks supported by b

            // count blocks supporting each of other
            // if > 1, b is removable
        });
        count
    }

    fn count_chain_reaction(&self) -> i32 {
        let mut count = 0;
        self.blocks
            .iter()
            .map(|b| {
                let mut removed: BTreeSet<usize> = BTreeSet::new();
                self.chain_reaction(b, &mut removed)
            })
            .sum()
    }

    fn chain_reaction(&self, b: &Block, removed: &mut BTreeSet<usize>) -> i32 {
        removed.insert(b.id);
        // println!("removed {:?}", removed);
        let others: Vec<&Block> = self
            .blocks
            .iter()
            .filter(|supported| supported.is_on(b))
            .filter(|supported| {
                self.blocks
                    .iter()
                    .filter(|support| supported.is_on(support))
                    .all(|support| removed.contains(&support.id))
            })
            .collect();
        // println!("{:?}", others);
        others
            .iter()
            .map(|supported| 1 + self.chain_reaction(supported, removed))
            .sum()
    }
}

impl fmt::Display for Stack {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let max_z_range = self.blocks.iter().last().unwrap().z_range();
        let max_z = max_z_range.end();
        for z in (1..*max_z).rev() {
            for y in 0..10 {
                for x in 0..10 {
                    let mut found_one = false;
                    for b in self.blocks.iter() {
                        if b.z_range().contains(&z)
                            && b.y_range().contains(&y)
                            && b.x_range().contains(&x)
                        {
                            found_one = true;
                            write!(f, "{:>5}", b.id).ok();
                        }
                    }
                    if !found_one {
                        write!(f, "{:>5}", '.').ok();
                    }
                }
                write!(f, "   ").ok();
            }
            write!(f, "\n").ok();
        }
        for y in 0..10 {
            for x in 0..10 {
                write!(f, "-").ok();
            }
            write!(f, "   ").ok();
        }
        write!(f, "\n")
    }
}
