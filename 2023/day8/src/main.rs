use lazy_regex::regex_captures;
use num::integer::lcm;
use std::collections::BTreeMap;

fn main() {
    let input = include_str!("../params.txt");

    let mut parts = input.split("\n\n");
    let instructions: Vec<char> = parts.next().unwrap().chars().collect();
    let mut network: BTreeMap<&str, (&str, &str)> = BTreeMap::new();
    for line in parts.next().unwrap().lines() {
        let (_, node, left, right) =
            regex_captures!(r#"(\w{3}) = \((\w{3}), (\w{3})\)"#, line).unwrap();
        network.insert(node, (left, right));
    }

    let mut loc = "AAA";
    let mut count = 0;
    while loc != "ZZZ" {
        let dir = instructions[count % instructions.len()];
        let next_locs = network.get(loc).unwrap();
        if dir == 'L' {
            loc = next_locs.0
        } else {
            loc = next_locs.1
        }
        count += 1;
    }

    println!("Part 1 - {}", count);

    let locs: Vec<&str> = network
        .keys()
        .filter(|node| ends_with(node, 'A'))
        .copied()
        .collect();
    
    let steps = locs
        .iter()
        .map(|starting_loc| {
            let mut loc = *starting_loc;
            let mut count = 0;
            while !ends_with(loc, 'Z') {
                let dir = instructions[count % instructions.len()];
                let next_locs = network.get(loc).unwrap();
                if dir == 'L' {
                    loc = next_locs.0
                } else {
                    loc = next_locs.1
                }
                count += 1;
            }
            count
        })
        .reduce(|a, b| lcm(a, b))
        .unwrap();
    println!("Part 2 - {}", steps);
}

fn ends_with(loc: &str, c: char) -> bool {
    loc.chars().last().unwrap() == c
}
