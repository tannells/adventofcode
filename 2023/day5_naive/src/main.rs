use itertools::Itertools;
use std::ops::Range;

fn main() {
    let input = include_str!("../example.txt");
    // let input = include_str!("../params.txt");

    let mut iter = input.split("\n\n");
    let seeds: Vec<usize> = iter
        .next()
        .unwrap()
        .split_whitespace()
        .skip(1)
        .map(|seed| seed.parse::<usize>().unwrap())
        .collect();

    let maps: Vec<GardenMap> = iter.map(|map| GardenMap::from_string(map)).collect();

    println!("Part 1 - {}", min_location(&seeds, &maps));

    let seed_ranges: Vec<std::ops::Range<usize>> = seeds
        .iter()
        .tuples()
        .map(|(start, length)| *start..(start + length))
        .collect();
    println!("ranges {:?}", seed_ranges);
    let seeds: Vec<usize> = seed_ranges
        .into_iter()
        .map(|range| range.into_iter())
        .flatten()
        .collect();
    // println!("seeds {:?}", seeds);
    traverse(&seeds, &maps);
    println!("Part 2 - {}", min_location(&seeds, &maps));
}

fn min_location(seeds: &Vec<usize>, maps: &Vec<GardenMap>) -> usize {
    seeds
        .iter()
        .map(|seed| {
            let mut val = *seed;
            for garden_map in maps {
                val = garden_map.destination(val);
                // println!("val: {}", val);
            }
            // print!("{}, ", val);
            val
        })
        .min()
        .unwrap()
}

fn traverse(seeds: &Vec<usize>, maps: &Vec<GardenMap>) {
    let mut vals = seeds.clone();
    vals.sort();
    println!("0: {:?}", vals);
    let mut j = 0;
    for map in maps {
        for i in 0..vals.len() {
            vals[i] = map.destination(vals[i])
        }
        vals.sort();
        j += 1;
        println!("{}: {:?}", j, vals);
    }
}

#[derive(Debug)]
struct GardenMap {
    name: String,
    ranges: Vec<Vec<usize>>,
}

impl GardenMap {
    fn new() -> GardenMap {
        return GardenMap {
            name: "".to_string(),
            ranges: Vec::new(),
        };
    }

    fn from_string(string: &str) -> GardenMap {
        let mut new_map = GardenMap::new();
        let mut iter = string.lines();
        new_map.name = iter.next().unwrap().to_string();
        new_map.ranges = iter
            .map(|line| {
                line.split_whitespace()
                    .map(|num| num.parse::<usize>().unwrap())
                    .collect()
            })
            .collect();
        return new_map;
    }

    fn destination(&self, source: usize) -> usize {
        for range in &self.ranges {
            let destination_start = range[0];
            let source_start = range[1];
            let range_length = range[2];
            let source_end = source_start + range_length - 1;
            if (source_start..=(source_end)).contains(&source) {
                return source + destination_start - source_start;
            }
        }
        return source;
    }
}
