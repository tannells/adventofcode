use std::cmp::Ordering;

fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let mut hands_and_bids: Vec<(&str, usize)> = input
        .lines()
        .map(|line| {
            let mut parts = line.split_whitespace();
            let hand = parts.next().unwrap();
            let bid = parts.next().unwrap().parse().unwrap();
            (hand, bid)
        })
        .collect();
    // println!("{:?}", hands_and_bids);

    rank_hands(&mut hands_and_bids, false);

    println!("Part 1 - {}", calculate_winnings(&hands_and_bids));

    rank_hands(&mut hands_and_bids, true);

    println!("Part 2 - {}", calculate_winnings(&hands_and_bids));
}

fn rank_hands(hands_and_bids: &mut Vec<(&str, usize)>, jwilds: bool) {
    hands_and_bids.sort_by(|(left_hand, _), (right_hand, _)| {
        let left_type = HandType::from(left_hand, jwilds);
        let right_type = HandType::from(right_hand, jwilds);
        match left_type.cmp(&right_type) {
            Ordering::Equal => {
                let left_strengths = card_strengths(left_hand, jwilds);
                let right_strengths = card_strengths(right_hand, jwilds);
                left_strengths.cmp(&right_strengths)
            }
            greater_or_less => greater_or_less,
        }
    });
}

fn calculate_winnings(hands_and_bids: &Vec<(&str, usize)>) -> usize {
    hands_and_bids
        .iter()
        .enumerate()
        .fold(0, |total, (rank, (_, bid))| total + (rank + 1) * bid)
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum HandType {
    HighCard,
    Pair,
    TwoPairs,
    Three,
    FullHouse,
    Four,
    Five,
}

impl HandType {
    fn from(hand: &str, jwilds: bool) -> HandType {
        let mut counts = [0; 13];
        let mut wilds_count = 0;
        for card in hand.chars() {
            counts[card_strength(card, jwilds)] += 1;
        }
        if jwilds {
            wilds_count = counts[0];
            counts[0] = 0;
        }
        counts.sort();
        counts.reverse();
        // println!("{:?}", counts);
        match (counts[0] + wilds_count, counts[1], counts[2]) {
            (5, _, _) => Self::Five,
            (4, _, _) => Self::Four,
            (3, 2, _) => Self::FullHouse,
            (3, _, _) => Self::Three,
            (2, 2, _) => Self::TwoPairs,
            (2, _, _) => Self::Pair,
            _ => Self::HighCard,
        }
    }

    fn strength(&self) -> u8 {
        *self as u8
    }
}

impl Ord for HandType {
    fn cmp(&self, other: &Self) -> Ordering {
        self.strength().cmp(&other.strength())
    }
}

impl PartialOrd for HandType {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.strength().cmp(&other.strength()))
    }
}

fn card_strengths(hand: &str, jwilds: bool) -> Vec<usize> {
    hand.chars().map(|c| card_strength(c, jwilds)).collect()
}

fn card_strength(card: char, jwilds: bool) -> usize {
    let order;
    if jwilds {
        order = [
            'J', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'Q', 'K', 'A',
        ]
    } else {
        order = [
            '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A',
        ]
    }
    order.iter().position(|c| c == &card).unwrap()
}
