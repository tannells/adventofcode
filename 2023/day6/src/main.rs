fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let mut rows = input.lines().map(|line| {
        line.split_whitespace()
            .skip(1)
            .map(|num| num.parse::<i32>().unwrap())
    });
    let times = rows.next().unwrap();
    let distances = rows.next().unwrap();

    let product = times.zip(distances)
        .map(|(time, distance_to_beat)| {
            (1..time)
                .map(|button_time| button_time * (time - button_time))
                .filter(|distance| distance > &distance_to_beat)
                .count()
        })
        .product::<usize>();

    println!("Part 1 - {}", product);

    let mut numbers = input
        .lines()
        .map(|line| {
            line.split_whitespace()
                .skip(1)
                .collect::<String>()
        })
        .map(|num| num.parse::<i64>().unwrap());
    let time = numbers.next().unwrap();
    let distance_to_beat = numbers.next().unwrap();
    let mut min_winning_time = 0;
    for button_time in 1..time {
        if button_time * (time - button_time) > distance_to_beat {
            min_winning_time = button_time;
            break;
        }
    }
    let ways_to_win = time - 2 * min_winning_time + 1;
    println!("Part 2 - {}", ways_to_win);
}
