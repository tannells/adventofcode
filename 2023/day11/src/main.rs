use std::cmp;

fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");
    let space: Vec<Vec<char>> = input.lines().map(|line| line.chars().collect()).collect();
    let rows = space.len();
    let cols = space[0].len();

    // empty rows
    let mut empty_rows = Vec::new();
    for y in 0..rows {
        if (0..cols).all(|x| space[y][x] == '.') {
            empty_rows.push(y);
        }
    }

    // empty cols
    let mut empty_cols = Vec::new();
    for x in 0..cols {
        if (0..rows).all(|y| space[y][x] == '.') {
            empty_cols.push(x);
        }
    }

    let mut galaxy_locs = Vec::new();
    for y in 0..space.len() {
        for x in 0..space[0].len() {
            // println!("({}, {})", x, y);
            if space[y][x] == '#' {
                galaxy_locs.push((x, y));
            }
        }
    }

    println!(
        "Part 1 - {}",
        total_distance_between_pairs(&galaxy_locs, &empty_rows, &empty_cols, 2)
    );
    println!(
        "Part 2 - {}",
        total_distance_between_pairs(&galaxy_locs, &empty_rows, &empty_cols, 1000000)
    );
}

fn total_distance_between_pairs(
    galaxy_locs: &Vec<(usize, usize)>,
    empty_rows: &Vec<usize>,
    empty_cols: &Vec<usize>,
    empty_distance: usize,
) -> usize {
    let mut distance_sum = 0;
    for i in 0..galaxy_locs.len() - 1 {
        for j in i + 1..galaxy_locs.len() {
            // println!("pair {} <-> {} {}", i, j, );
            distance_sum += distance(
                galaxy_locs[i],
                galaxy_locs[j],
                &empty_rows,
                &empty_cols,
                empty_distance,
            );
        }
    }
    distance_sum
}

fn distance(
    a: (usize, usize),
    b: (usize, usize),
    empty_rows: &Vec<usize>,
    empty_cols: &Vec<usize>,
    empty_distance: usize,
) -> usize {
    let left = cmp::min(a.0, b.0);
    let right = cmp::max(a.0, b.0);
    let top = cmp::min(a.1, b.1);
    let bottom = cmp::max(a.1, b.1);

    let mut distance = 0;

    let empty_rows_crossed = (left..right).filter(|col| empty_cols.contains(col)).count();
    let horizontal_distance = right - left + (empty_distance - 1) * empty_rows_crossed;
    distance += horizontal_distance;

    let empty_cols_crossed = (top..bottom).filter(|row| empty_rows.contains(row)).count();
    let vertical_distance = bottom - top + (empty_distance - 1) * empty_cols_crossed;
    distance += vertical_distance;

    // println!("{:?} - {:?} = {}", a, b, distance);
    distance
}
