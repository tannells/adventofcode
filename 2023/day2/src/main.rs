use std::cmp;

use lazy_regex::regex_captures;

fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let sum = input.lines().fold(0, |sum, line| {
        let (_, id, mut handfuls) = regex_captures!(r#"Game (\d*): (.*)"#, line).unwrap();
        // println!("game id: {}", id);
        let mut quantity;
        let mut color;
        let mut possible = true;
        while handfuls.len() > 0 {
            (_, quantity, color, handfuls) =
                regex_captures!(r#"(\d*) (red|green|blue)(.*)"#, handfuls).unwrap();

            // println!("quantity: {} color: {}", quantity, color);
            let color_index = ["red", "green", "blue"]
                .iter()
                .position(|&col| col == color)
                .unwrap();

            let max = [12, 13, 14][color_index];
            possible = possible && max >= quantity.parse::<usize>().unwrap();
        }
        if possible {
            sum + id.parse::<usize>().unwrap()
        } else {
            sum
        }
    });
    println!("Part 1 - {}", sum);

    let power = input.lines().fold(0, |sum, line| {
        let (_, _, mut handfuls) = regex_captures!(r#"Game (\d*): (.*)"#, line).unwrap();
        let mut quantity;
        let mut color;
        let mut max_array = [0, 0, 0];
        while handfuls.len() > 0 {
            (_, quantity, color, handfuls) =
                regex_captures!(r#"(\d*) (red|green|blue)(.*)"#, handfuls).unwrap();

            let color_index = ["red", "green", "blue"]
                .iter()
                .position(|&col| col == color)
                .unwrap();

            max_array[color_index] =
                cmp::max(max_array[color_index], quantity.parse::<usize>().unwrap());
        }
        sum + max_array.iter().product::<usize>()
    });
    println!("Part 2 - {}", power);
}
