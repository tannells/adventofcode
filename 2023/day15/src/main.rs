use lazy_regex::regex_captures;
use std::collections::BTreeMap;
use std::fmt;

fn main() {
    let input = include_str!("../example.txt");
    // let input = include_str!("../params.txt");

    let sum: u32 = input.split(',').map(|s| hash(s)).sum();

    println!("Part 1 - {}", sum);

    let mut boxes: BTreeMap<u32, Vec<Lens>> = BTreeMap::new();

    input.split(',').for_each(|str| {
        let (_, label, op) = regex_captures!(r#"(\w+)(-|=\d)"#, str).unwrap();
        let box_index = hash(label);
        // println!("box_index: {}", box_index);
        if op.starts_with('-') {
            if boxes.contains_key(&box_index) {
                let b = boxes.get_mut(&box_index).unwrap();
                if let Some(p) = b.iter().position(|l| l.label == label) {
                    b.remove(p);
                    if b.len() == 0 {
                        boxes.remove(&box_index);
                    }
                }
            }
        } else {
            // starts with =
            let focal_length = op.chars().skip(1).next().unwrap().to_digit(10).unwrap();
            let new_lens = Lens {
                label: label.to_string(),
                focal_length: focal_length,
            };
            if !boxes.contains_key(&box_index) {
                boxes.insert(box_index, Vec::new());
            }
            let b = boxes.get_mut(&box_index).unwrap();
            if let Some(p) = b.iter().position(|l| l.label == label) {
                b.remove(p);
                b.insert(p, new_lens);
            } else {
                b.push(new_lens);
            }
        }
        // println!("{:?}", boxes);
    });

    let focusing_power: u32 = boxes
        .into_iter()
        .map(|(box_index, Lens)| {
            Lens.iter()
                .enumerate()
                .map(|(lens_index, l)| (1 + box_index) * (lens_index as u32 + 1) * l.focal_length)
                .sum::<u32>()
        })
        .sum();

    println!("Part 2 - {}", focusing_power);
}

fn hash(string: &str) -> u32 {
    let mut val = 0;
    string.chars().for_each(|c| {
        let ascii = c as u32;
        val += ascii;
        val *= 17;
        val %= 256;
    });
    // println!("{}", val);
    val
}

struct Lens {
    label: String,
    focal_length: u32,
}

impl fmt::Debug for Lens {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{} {}]", self.label, self.focal_length)
    }
}
