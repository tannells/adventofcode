use itertools::Itertools;
use range_collections::range_set::RangeSet;
use std::ops::{
    Bound::{Excluded, Included},
    RangeBounds,
};

const SIZE: usize = 2048;

fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let mut iter = input.split("\n\n");
    let seeds: Vec<usize> = iter
        .next()
        .unwrap()
        .split_whitespace()
        .skip(1)
        .map(|seed| seed.parse::<usize>().unwrap())
        .collect();

    let maps: Vec<GardenMap> = iter.map(|map| GardenMap::from_string(map)).collect();

    println!("Part 1 - {}", min_location(&seeds, &maps));

    let seed_ranges: RangeSet<[usize; SIZE]> = seeds.iter().tuples().fold(
        RangeSet::<[usize; SIZE]>::empty(),
        |range, (start, length)| {
            range.union(&RangeSet::<[usize; SIZE]>::from(*start..(start + length)))
        },
    );
    // println!("0: {:?}", seed_ranges);

    println!("Part 2 - {}", min_location2(seed_ranges, &maps));
}

fn min_location(seeds: &Vec<usize>, maps: &Vec<GardenMap>) -> usize {
    seeds
        .iter()
        .map(|seed| {
            let mut val = *seed;
            for garden_map in maps {
                val = garden_map.destination(val);
                // println!("val: {}", val);
            }
            // println!("location: {}", val);
            val
        })
        .min()
        .unwrap()
}

fn min_location2(seeds: RangeSet<[usize; SIZE]>, maps: &Vec<GardenMap>) -> usize {
    let mut val = seeds.clone();
    // let mut i = 0;
    for garden_map in maps {
        val = garden_map.destination_ranges(val);
        // i += 1;
        // println!("{}: {:?}", i, val);
    }
    *val.boundaries().first().unwrap()
}

#[derive(Debug)]
struct GardenMap {
    name: String,
    ranges: Vec<Vec<usize>>,
}

impl GardenMap {
    fn new() -> GardenMap {
        return GardenMap {
            name: "".to_string(),
            ranges: Vec::new(),
        };
    }

    fn from_string(string: &str) -> GardenMap {
        let mut new_map = GardenMap::new();
        let mut iter = string.lines();
        new_map.name = iter.next().unwrap().to_string();
        new_map.ranges = iter
            .map(|line| {
                line.split_whitespace()
                    .map(|num| num.parse::<usize>().unwrap())
                    .collect()
            })
            .collect();
        return new_map;
    }

    fn destination(&self, source: usize) -> usize {
        for range in &self.ranges {
            let destination_start = range[0];
            let source_start = range[1];
            let range_length = range[2];
            let source_end = source_start + range_length - 1;
            if (source_start..=(source_end)).contains(&source) {
                return source + destination_start - source_start;
            }
        }
        return source;
    }

    fn destination_ranges(
        &self,
        source_ranges: RangeSet<[usize; SIZE]>,
    ) -> RangeSet<[usize; SIZE]> {
        let mut destination_ranges = RangeSet::<[usize; SIZE]>::empty();
        let mut remaining = source_ranges.clone();

        for range in &self.ranges {
            let destination_start = range[0];
            let source_start = range[1];
            let range_length = range[2];
            let source_end = source_start + range_length - 1;
            let source_range = RangeSet::<[usize; SIZE]>::from(source_start..source_end + 1);
            if remaining.intersects(&source_range) {
                let shifted_range = source_ranges
                    .intersection::<[usize; SIZE]>(&source_range)
                    .iter()
                    .map(|range| {
                        if let Included(start) = range.start_bound() {
                            if let Excluded(end) = range.end_bound() {
                                // println!("source range  {:?}", range);
                                // println!("shifted range {:?}", (**start + destination_start - source_start)
                                //     ..(**end + destination_start - source_start));
                                return RangeSet::<[usize; SIZE]>::from(
                                    (**start + destination_start - source_start)
                                        ..(**end + destination_start - source_start),
                                );
                            }
                        }
                        RangeSet::<[usize; SIZE]>::empty()
                    })
                    .fold(RangeSet::<[usize; SIZE]>::empty(), |full_range, range| {
                        full_range.union(&range)
                    });
                destination_ranges.union_with(&shifted_range);
                remaining.difference_with(&source_range);
            }
        }
        destination_ranges.union(&remaining)
    }
}
