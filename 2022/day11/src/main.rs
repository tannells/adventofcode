#[derive(Debug, Clone)]
struct Monkey {
    items: Vec<u64>,
    operator: String,
    operand: String,
    test_operand: u64,
    throw_if_true: usize,
    throw_if_false: usize,
}

impl Monkey {
    fn new() -> Monkey {
        return Monkey {
            items: Vec::new(),
            operator: "".to_string(),
            operand: "".to_string(),
            test_operand: 0,
            throw_if_true: 0,
            throw_if_false: 0,
        };
    }

    fn parse_notes(monkey_notes: Vec<&str>) -> Vec<Monkey> {
        let mut monkeys = Vec::new();
        for note in monkey_notes {
            let mut monkey = Monkey::new();
            let mut lines = note.lines();
            lines.next();
            monkey.items = lines
                .next()
                .unwrap()
                .split(":")
                .last()
                .unwrap()
                .split(",")
                .map(|x| x.trim().parse::<u64>().unwrap())
                .collect::<Vec<u64>>();
            let operation = lines
                .next()
                .unwrap()
                .split("new = ")
                .last()
                .unwrap()
                .split(" ")
                .collect::<Vec<&str>>();
            monkey.operator = operation[1].to_string();
            monkey.operand = operation[2].to_string();
            monkey.test_operand = lines
                .next()
                .unwrap()
                .split(" ")
                .last()
                .unwrap()
                .parse::<u64>()
                .unwrap();
            monkey.throw_if_true = lines
                .next()
                .unwrap()
                .split(" ")
                .last()
                .unwrap()
                .parse::<usize>()
                .unwrap();
            monkey.throw_if_false = lines
                .next()
                .unwrap()
                .split(" ")
                .last()
                .unwrap()
                .parse::<usize>()
                .unwrap();

            // println!("{:?}", monkey);
            monkeys.push(monkey);
        }
        return monkeys;
    }

    fn apply_operation(&self, mut item: u64) -> u64 {
        let operand;
        match self.operand.as_str() {
            "old" => operand = item,
            other => operand = other.parse::<u64>().unwrap(),
        }
        match self.operator.as_str() {
            "+" => item += operand,
            "*" => item *= operand,
            _ => {}
        }
        return item;
    }

    fn throw_to(&self, item: u64) -> usize {
        if item % self.test_operand == 0 {
            return self.throw_if_true;
        }
        return self.throw_if_false;
    }
}

fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");
    let monkey_notes = input.split("\n\n").collect::<Vec<&str>>();

    let initial_monkeys = Monkey::parse_notes(monkey_notes);

    // part 1
    let mut monkeys = initial_monkeys.to_vec();
    let mut inspects: Vec<u64> = vec![0; monkeys.len()];
    let rounds_1 = 20;

    for _ in 0..rounds_1 {
        for i in 0..monkeys.len() {
            let items_to_throw: Vec<u64>;
            {
                let monkey = &mut monkeys[i];
                for j in 0..monkey.items.len() {
                    monkey.items[j] = monkey.apply_operation(monkey.items[j]) / 3;
                    inspects[i] += 1;
                }
                items_to_throw = monkey.items.drain(..).collect();
            }
            for item in items_to_throw {
                let throw_to = monkeys[i].throw_to(item);
                monkeys[throw_to].items.push(item);
            }
        }
    }

    inspects.sort_by(|a, b| b.cmp(a));

    // println!("{:?}", monkeys);
    // println!("{:?}", inspects);

    println!("monkey business 1 = {}", inspects[0] * inspects[1]);

    // part 2
    monkeys = initial_monkeys;
    inspects = vec![0; monkeys.len()];
    let rounds_2 = 10000;
    // divisors are all prime, so least common multiple is just the product
    let least_common_multiple = monkeys
        .iter()
        .map(|monkey| monkey.test_operand)
        .fold(1, |product, x| product * x);
    // println!("least common multiple {}", least_common_multiple);

    for _ in 0..rounds_2 {
        for i in 0..monkeys.len() {
            let items_to_throw: Vec<u64>;
            {
                let monkey = &mut monkeys[i];
                for j in 0..monkey.items.len() {
                    monkey.items[j] =
                        monkey.apply_operation(monkey.items[j]) % least_common_multiple;
                    inspects[i] += 1;
                }
                items_to_throw = monkey.items.drain(..).collect();
            }
            for item in items_to_throw {
                let throw_to = monkeys[i].throw_to(item);
                monkeys[throw_to].items.push(item);
            }
        }
    }

    inspects.sort_by(|a, b| b.cmp(a));

    // println!("{:?}", monkeys);
    // println!("{:?}", inspects);

    println!("monkey business 2 = {}", inspects[0] * inspects[1]);
}
