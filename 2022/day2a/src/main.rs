use std::collections::HashMap;

fn main() {
    let strategy_guide = include_str!("../params.txt");
    // println!("{}", strategy_guide);
    let rounds = strategy_guide.lines();

    let abc = ["A", "B", "C"];
    let xyz = ["X", "Y", "Z"];
    let rps = ["Rock", "Paper", "Scissors"];

    let mut move_lookup = HashMap::new();
    for i in 0..=2 {
        move_lookup.insert(abc[i], rps[i]);
        move_lookup.insert(xyz[i], rps[i]);
    }

    let mut winner_lookup = HashMap::new();
    for first_move in rps {
        let mut result = HashMap::new();
        for second_move in rps {
            let first_index = rps.iter().position(|&x| x == first_move).unwrap() as i8;
            let second_index = rps.iter().position(|&x| x == second_move).unwrap() as i8;
            // rem_euclid is mod (least nonnegative remainder)
            let score = (second_index - first_index + 1).rem_euclid(3);
            result.insert(second_move, score);
        }
        winner_lookup.insert(first_move, result);
    }
    // println!("{:?}", winner_lookup);

    let mut total_score = 0;
    for round in rounds {
        let moves = round.split(" ").collect::<Vec<_>>();
        let opponent_move = move_lookup.get(moves[0]).unwrap();
        // println!("{}", opponent_move);
        let my_move = move_lookup.get(moves[1]).unwrap();
        // println!("{}", my_move);
        let shape_score = rps.iter().position(|&x| x == *my_move).unwrap() as i8 + 1;
        // println!("{}", shape_score);
        let outcome_score = winner_lookup
            .get(opponent_move)
            .unwrap()
            .get(my_move)
            .unwrap()
            * 3;
        // println!("{}", outcome_score);
        total_score += shape_score as i32 + outcome_score as i32;
    }

    println!("{}", total_score);
}
