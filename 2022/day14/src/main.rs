use std::cmp;
use std::fmt;

struct Cave {
    plane: Vec<Vec<Matter>>,
    origin: (usize, usize),
    max: (usize, usize),
}

#[derive(Clone, PartialEq)]
enum Matter {
    Air,
    Rock,
    Sand,
}

impl Cave {
    fn new() -> Cave {
        return Cave {
            plane: Vec::new(),
            origin: (0, 0),
            max: (0, 0),
        };
    }

    fn rows(&self) -> usize {
        return self.plane.len();
    }

    fn columns(&self) -> usize {
        return self.plane.first().unwrap().len();
    }

    fn get(&self, x: usize, y: usize) -> Matter {
        return self.plane[y][x - self.origin.0].clone();
    }

    fn set(&mut self, x: usize, y: usize, val: Matter) {
        self.plane[y][x - self.origin.0] = val;
    }

    fn from(string: &str) -> Cave {
        let paths = Cave::get_paths(string);
        let mut cave = Cave::new();
        cave.initialize_plane(&paths);
        cave.place_rocks(&paths);
        // println!("{}", cave);
        return cave;
    }

    fn get_paths(string: &str) -> Vec<Vec<(usize, usize)>> {
        let mut paths = Vec::new();
        for line in string.lines() {
            let mut path = Vec::new();
            let points = line.split(" -> ");
            for point in points {
                let mut coordinates = point.split(",");
                let x: usize = coordinates.next().unwrap().parse().unwrap();
                let y: usize = coordinates.next().unwrap().parse().unwrap();
                path.push((x, y));
            }
            paths.push(path);
        }
        return paths;
    }

    fn initialize_plane(&mut self, paths: &Vec<Vec<(usize, usize)>>) {
        self.max.1 = paths.iter().fold(0, |max_y, path| {
            cmp::max(
                max_y,
                path.iter().fold(0, |max_y, point| cmp::max(max_y, point.1)),
            )
        });
        self.max.1 += 2;
        self.origin = (500 - self.max.1 - 1, 0);
        self.max.0 = 500 + self.max.1 + 1;

        // println!("origin {:?} max {:?}", self.origin, self.max);

        let row = vec![Matter::Air; self.max.0 - self.origin.0 + 1];
        self.plane = vec![row; self.max.1 - self.origin.1 + 1];
    }

    fn place_rocks(&mut self, paths: &Vec<Vec<(usize, usize)>>) {
        for path in paths {
            let mut points = path.iter();
            let mut previous = points.next().unwrap();
            for point in points {
                // println!("{:?} -> {:?}", previous, point);
                let x_start = cmp::min(previous.0, point.0);
                let x_end = cmp::max(previous.0, point.0);
                let y_start = cmp::min(previous.1, point.1);
                let y_end = cmp::max(previous.1, point.1);
                for x in x_start..=x_end {
                    for y in y_start..=y_end {
                        self.set(x, y, Matter::Rock);
                    }
                }
                previous = point;
            }
        }
    }

    fn add_floor(&mut self) {
        let y = self.rows() - 1;
        for x in 0..self.columns() {
            self.plane[y][x] = Matter::Rock;
        }
    }

    fn fill_with_sand(&mut self) {
        while self.drop_sand() {}
    }

    fn drop_sand(&mut self) -> bool {
        let mut x = 500;
        for y in 0..self.rows() - 1 {
            // println!("({}, {})", x, y);
            // println!{"{} {} {}", x, x-1, self.origin.0};
            if self.get(x, y + 1) == Matter::Air {
            } else if x == self.origin.0 {
                return false;
            } else if self.get(x - 1, y + 1) == Matter::Air {
                x -= 1;
            } else if x == self.max.0 {
                return false;
            } else if self.get(x + 1, y + 1) == Matter::Air {
                x += 1;
            } else {
                self.set(x, y, Matter::Sand);
                return y != 0;
            }
        }
        return false;
    }

    fn count_sand(&self) -> u32 {
        self.plane.iter().fold(0, |sum, row| {
            sum + row.iter().fold(
                0,
                |sum, val| if *val == Matter::Sand { sum + 1 } else { sum },
            )
        })
    }
}

impl fmt::Display for Cave {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for row in &self.plane {
            for val in row {
                let char = match val {
                    Matter::Air => '.',
                    Matter::Rock => '#',
                    Matter::Sand => 'o',
                };
                write!(f, "{}", char)?;
            }
            write!(f, "\n")?
        }
        Ok(())
    }
}

fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");
    let mut cave = Cave::from(input);
    cave.fill_with_sand();
    println!("{}", cave);
    println!("{} sand", cave.count_sand());

    cave = Cave::from(input);
    cave.add_floor();
    cave.fill_with_sand();

    println!("{}", cave);
    println!("{} sand", cave.count_sand());
}
