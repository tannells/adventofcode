use core::str::Chars;
use std::cmp::Ordering;
use std::fmt;
use std::iter::Peekable;

#[derive(Debug, Clone, Eq, PartialEq)]
enum Packet {
    List(Vec<Packet>),
    Integer(u8),
}

impl Packet {
    fn new() -> Packet {
        return Packet::List(Vec::new());
    }

    fn from(string: &str) -> Packet {
        let mut chars = string.chars().peekable();
        match chars.peek() {
            Some(char) => match char {
                '[' => {
                    chars.next();
                    return Packet::from_chars(&mut chars);
                }
                '0'..='9' => {
                    return Packet::Integer(Self::parse_number(&mut chars));
                }
                _ => return Packet::new(),
            },
            None => return Packet::new(),
        }
    }

    fn from_chars(chars: &mut Peekable<Chars>) -> Packet {
        let mut packets = Vec::new();
        while let Some(char) = chars.peek() {
            match char {
                '[' => {
                    chars.next();
                    packets.push(Packet::from_chars(chars));
                }
                ']' => {
                    chars.next();
                    return Packet::List(packets);
                }
                '0'..='9' => {
                    packets.push(Packet::Integer(Self::parse_number(chars)));
                }
                _ => {
                    chars.next();
                }
            }
        }
        return Packet::List(packets);
    }

    fn parse_number(chars: &mut Peekable<Chars>) -> u8 {
        let mut digits = Vec::new();
        while let Some(char) = chars.peek() {
            if char.is_digit(10) {
                digits.push(chars.next().unwrap())
            } else {
                break;
            }
        }
        return digits.iter().collect::<String>().parse::<u8>().unwrap();
    }
}

impl Ord for Packet {
    fn cmp(&self, other: &Self) -> Ordering {
        match self {
            Packet::List(list) => match other {
                Packet::List(other_list) => {
                    for i in 0..list.len().min(other_list.len()) {
                        let ordering = list[i].cmp(&other_list[i]);
                        if ordering == Ordering::Equal {
                            continue;
                        }
                        return ordering;
                    }
                    list.len().cmp(&other_list.len())
                }
                Packet::Integer(_) => self.cmp(&Packet::List(vec![other.clone()])),
            },
            Packet::Integer(integer) => match other {
                Packet::List(_) => Packet::List(vec![self.clone()]).cmp(other),
                Packet::Integer(other_integer) => integer.cmp(other_integer),
            },
        }
    }
}

impl PartialOrd for Packet {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl fmt::Display for Packet {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Packet::List(list) => {
                write!(f, "[")?;
                match list.len() {
                    0 => {
                        write!(f, "")?;
                    }
                    1 => {
                        write!(f, "{}", list[0])?;
                    }
                    other => {
                        for i in 0..(other - 1) {
                            write!(f, "{}, ", list[i])?;
                        }
                        write!(f, "{}", list[other - 1])?;
                    }
                }
                write!(f, "]")?;
                Ok(())
            }
            Packet::Integer(integer) => write!(f, "{}", integer),
        }
    }
}

fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let mut index = 0;
    let mut correct_indices_sum = 0;
    let mut all_packets = Vec::new();
    for pair in input.split("\n\n") {
        index += 1;
        let mut packets = pair.lines();
        let left = Packet::from(packets.next().unwrap());
        let right = Packet::from(packets.next().unwrap());

        // println!("{} < {}?", left, right);
        // println!("{}", left < right);
        if left < right {
            correct_indices_sum += index
        }

        all_packets.push(left);
        all_packets.push(right);
    }
    println!("correct indices sum = {}", correct_indices_sum);

    let div2 = Packet::from("[[2]]");
    let div6 = Packet::from("[[6]]");

    all_packets.push(div2.clone());
    all_packets.push(div6.clone());
    all_packets.sort();
    // for packet in all_packets { println!{"{}", packet}; }

    let pos1 = all_packets
        .iter()
        .position(|packet| packet == &div2)
        .unwrap()
        + 1;
    let pos2 = all_packets
        .iter()
        .position(|packet| packet == &div6)
        .unwrap()
        + 1;
    // println!{"pos1 {} pos2 {}", pos1, pos2};
    println!("decoder key = {}", pos1 * pos2);
}
