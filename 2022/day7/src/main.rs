use ::std::collections::HashMap;

#[derive(Debug)]
struct Directory {
    subdirectories: HashMap<String, Directory>,
    file_content_size: u32,
}

impl Directory {
    fn get_dir_mut(&mut self, path: &Vec<&str>) -> &mut Directory {
        let mut current_dir = self;
        for folder in path {
            current_dir = current_dir.subdirectories.get_mut(*folder).unwrap();
        }
        return current_dir;
    }

    fn total_size(&self) -> u32 {
        return self.file_content_size
            + self
                .subdirectories
                .iter()
                .fold(0, |sum, (_k, v)| sum + v.total_size());
    }

    fn directories_smaller_than(&self, size: u32) -> Vec<&Directory> {
        let mut smaller_directories: Vec<&Directory> = self
            .subdirectories
            .iter()
            .filter(|(_k, v)| v.total_size() <= size)
            .map(|(_k, v)| v)
            .collect();
        for (_name, subdirectory) in &self.subdirectories {
            smaller_directories.extend_from_slice(&subdirectory.directories_smaller_than(size));
        }
        return smaller_directories;
    }

    fn directories_larger_than(&self, size: u32) -> Vec<&Directory> {
        let mut larger_directories: Vec<&Directory> = self
            .subdirectories
            .iter()
            .filter(|(_k, v)| v.total_size() >= size)
            .map(|(_k, v)| v)
            .collect();
        for (_name, subdirectory) in &self.subdirectories {
            larger_directories.extend_from_slice(&subdirectory.directories_larger_than(size));
        }
        return larger_directories;
    }
}

fn main() {
    let mut root_dir: Directory = Directory {
        subdirectories: HashMap::new(),
        file_content_size: 0,
    };
    let mut path: Vec<&str> = Vec::new();
    let mut current_dir = &mut root_dir;

    let terminal_output = include_str!("../params.txt").lines();
    for line in terminal_output {
        let mut args = line.split(" ");
        match args.next().unwrap() {
            "$" => {
                // println!("user input");
                match args.next().unwrap() {
                    "cd" => {
                        match args.next().unwrap() {
                            "/" => {
                                // println!("switch to outermost directory");
                                path = Vec::new();
                            }
                            ".." => {
                                // println!("move out one level");
                                path.pop();
                            }
                            other => {
                                // println!("move in one level to {}", other);
                                path.push(other);
                            }
                        }
                        current_dir = root_dir.get_dir_mut(&path);
                    }
                    "ls" => {
                        // println!("list")
                    }
                    _ => {}
                }
            }
            "dir" => {
                // println!("directory");
                let dir_name = args.next().unwrap();
                if current_dir.subdirectories.get(dir_name).is_none() {
                    current_dir.subdirectories.insert(
                        dir_name.to_string(),
                        Directory {
                            subdirectories: HashMap::new(),
                            file_content_size: 0,
                        },
                    );
                }
            }
            other => {
                // println!("{}", other);
                let file_size = other.parse::<u32>().unwrap();
                current_dir.file_content_size += file_size;
            }
        }
    }

    // println!("{:?}", root_dir);
    // println!{"{}", root_dir.total_size()};
    println! {"sum of total size of directories smaller than 100000 = {}",
        root_dir.directories_smaller_than(100000)
            .iter()
            .fold(0, |sum, dir|
                sum + dir.total_size()
            )
    };

    let total_disk_space = 70000000;
    let required_free_space = 30000000;
    let free_space = total_disk_space - root_dir.total_size();
    let additional_space = required_free_space - free_space;
    // println!{"{}", additional_space};

    println! {"size of smallest directory to delete = {}",
        root_dir.directories_larger_than(additional_space)
            .iter()
            .map(|dir| dir.total_size())
            .min()
            .unwrap()
    };
}
