fn main() {
    let lines = include_str!("../params.txt").lines();
    let mut max = 0;
    let mut sum = 0;
    for line in lines {
        if line != "" {
            sum += line.parse::<u32>().unwrap();
        }
        else {
            if sum > max {
                max = sum
            }
            sum = 0
        }
    }
    println!("{}", max);
}
