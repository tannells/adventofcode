use std::collections::BTreeSet;

fn main() {
    // let motions = include_str!("../example.txt").lines();
    let motions = include_str!("../example2.txt").lines();
    // let motions = include_str!("../params.txt").lines();

    let knots = 10;

    let mut locations = vec![(0, 0); knots];
    let mut tail_locations: BTreeSet<(i32, i32)> = BTreeSet::new();
    tail_locations.insert(*locations.last().unwrap());

    // println!("head {:?} tail {:?}", head_location, tail_location);

    for motion in motions {
        let mut delta = (0, 0);
        let mut parts = motion.split(" ");
        match parts.next().unwrap() {
            "U" => delta = (0, -1),
            "R" => delta = (1, 0),
            "D" => delta = (0, 1),
            "L" => delta = (-1, 0),
            _ => {}
        }
        let distance = parts.next().unwrap().parse::<i32>().unwrap();

        for _ in 0..distance {
            let mut iter = locations.iter_mut();
            let mut prev = iter.next().unwrap();

            prev.0 += delta.0;
            prev.1 += delta.1;

            for location in iter {
                // println!("prev {:?} location {:?}", prev, location);
                let dx = prev.0 - location.0;
                let dy = prev.1 - location.1;

                if dx.abs() == 2 && dy == 0 {
                    // straight left/right
                    location.0 += dx.signum();
                } else if dx == 0 && dy.abs() == 2 {
                    // straight up/down
                    location.1 += dy.signum();
                } else if dx.abs() + dy.abs() > 2 {
                    // diagonal
                    location.0 += dx.signum();
                    location.1 += dy.signum();
                }

                prev = location;
            }
            tail_locations.insert(*locations.last().unwrap());
        }
        // println!("{:?}", locations);
    }

    println! {"tail locations {}", tail_locations.len()};
}
