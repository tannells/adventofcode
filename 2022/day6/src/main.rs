use std::collections::HashSet;

fn main() {
    let datastream = include_str!("../params.txt");
    // println!("{}", datastream);

    println!(
        "first start sequence at {}",
        first_unique_n_chars(4, datastream)
    );
    println!(
        "first message sequence at {}",
        first_unique_n_chars(14, datastream)
    );
}

fn first_unique_n_chars(n: usize, string: &str) -> i32 {
    let mut rolling_buffer: Vec<char> = Vec::new();
    let mut next = 0;
    let size = n;
    for _ in 0..size {
        rolling_buffer.push(' ');
    }

    let mut position = 0;
    for char in string.chars() {
        rolling_buffer[next] = char;
        next = (next + 1) % size;
        position += 1;
        if (&rolling_buffer).into_iter().any(|x| *x == ' ') {
            continue;
        }
        // println!("{:?}", rolling_buffer);
        let unique_chars = rolling_buffer.iter().collect::<HashSet<&char>>().len();
        // println!("{}", unique_chars);
        if unique_chars == size {
            break;
        }
    }
    return position;
}
