fn main() {
    let rucksacks: Vec<&str> = include_str!("../params.txt").lines().collect();

    let mut rearrange_sum = 0;
    for rucksack in &rucksacks {
        let items: Vec<char> = rucksack.chars().collect();
        let rucksack_size = rucksack.chars().count();
        let compartment_size = rucksack_size / 2;

        for i in 0..compartment_size {
            let mut found = false;
            for j in compartment_size..rucksack_size {
                if items[i] == items[j] {
                    // println!("{}", items[i]);
                    found = true;
                    rearrange_sum += item_priority(items[i]);
                    break;
                }
            }
            if found {
                break;
            }
        }
    }
    println!("rearrange {}", rearrange_sum);

    let mut badge_sum = 0;
    for i in 0..rucksacks.len() / 3 {
        for item in rucksacks[3 * i].chars() {
            if rucksacks[3 * i + 1].chars().any(|x| x == item)
                && rucksacks[3 * i + 2].chars().any(|x| x == item)
            {
                // println!("{}", item);
                badge_sum += item_priority(item);
                break;
            }
        }
    }

    println!("badge {}", badge_sum);
}

fn item_priority(item: char) -> i32 {
    let mut priority = item as i32 - 'a' as i32 + 1;
    if priority < 0 {
        priority += 58;
    }
    // println!("{}", priority);
    return priority;
}
