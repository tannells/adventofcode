fn main() {
    // let instructions = include_str!("../example.txt").lines();
    let instructions = include_str!("../params.txt").lines();

    let mut x = 1;
    let mut history: Vec<i32> = Vec::new();

    for instruction in instructions {
        let mut parts = instruction.split(" ");
        match parts.next().unwrap() {
            "addx" => {
                for _ in 0..2 {
                    history.push(x)
                }
                x += parts.next().unwrap().parse::<i32>().unwrap();
            }
            "noop" => history.push(x),
            _ => {}
        }
    }
    // println!{"{:?}", history};

    let mut sum = 0;
    for i in [20, 60, 100, 140, 180, 220] {
        sum += i32::try_from(i).unwrap() * history[i - 1];
    }
    println! {"signal strength sum {}", sum};

    for i in 0..240 {
        if i % 40 == 0 {
            print!("\n")
        }

        if (history[i] - i32::try_from(i % 40).unwrap()).abs() <= 1 {
            print! {"#"}
        } else {
            print! {"."}
        }
    }
}
