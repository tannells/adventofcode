use lazy_static::lazy_static;
use regex::Regex;
use std::collections::{BTreeMap, BTreeSet};

#[derive(Debug)]
struct Graph {
    current_location: String,
    nodes: BTreeMap<String, Node>,
    time_remaining: u32,
}

// ideas
// - simplify graph by removing 0-flow nodes and using edges with distances
// - treat opening valve as edge from to itself
// - find optimal order to open the next n valves
//   - open all permutations of n valves
//   - choose the best order
//     - choose n instead of just 1
//     - figure out which one's the best
// - prune order by only trying the biggest or highest potentials valves

impl Graph {
    fn new() -> Graph {
        Graph {
            current_location: "AA".to_string(),
            nodes: BTreeMap::new(),
            time_remaining: 30,
        }
    }
    fn parse(input: &str) -> Graph {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"Valve ([A-Z]{2}) has flow rate=(\d*); tunnels? leads? to valves? ([A-Z]{2}(, [A-Z]{2})*)").unwrap();
        }

        let mut graph = Graph::new();
        RE.captures_iter(input).for_each(|cap| {
            graph.insert(Node {
                name: cap[1].to_string(),
                adjacent_nodes: cap[3]
                    .split(", ")
                    .map(|edge| edge.to_string())
                    .collect::<Vec<String>>(),
                valve_flow: cap[2].parse().unwrap(),
                valve_open: false,
            });
        });
        // println!("new graph: {:?}", graph);
        return graph;
    }

    fn insert(&mut self, node: Node) {
        self.nodes.insert(node.name.clone(), node);
    }

    fn distances_to_nodes(&self) -> BTreeMap<&String, u32> {
        let mut visited = BTreeSet::new();
        let mut distances_to_nodes = BTreeMap::new();
        visited.insert(&self.current_location);
        let mut paths = vec![vec![&self.current_location]];
        while !paths.is_empty() {
            let mut new_paths = Vec::new();
            for path in paths {
                let current_location = path.last().unwrap();
                let current_node = self.nodes.get(current_location.as_str()).unwrap();
                for adjacent_node in &current_node.adjacent_nodes {
                    match visited.get(adjacent_node) {
                        Some(_) => {
                            continue;
                        }
                        None => {
                            visited.insert(adjacent_node);
                            distances_to_nodes.insert(adjacent_node, path.len() as u32);

                            let mut new_path = path.clone();
                            new_path.push(adjacent_node);

                            new_paths.push(new_path);
                        }
                    }
                }
            }
            paths = new_paths;
        }
        return distances_to_nodes;
    }

    fn max_pressure_releasable(&self) -> u32 {
        return 0;
    }
}

#[derive(Debug)]
struct Node {
    name: String,
    adjacent_nodes: Vec<String>,
    valve_flow: u32,
    valve_open: bool,
}

fn main() {
    let example = include_str!("../example.txt");
    let params = include_str!("../params.txt");

    run(example);
    run(params);
}

fn run(input: &str) {
    let graph = Graph::parse(input);
    let nonzero_nodes = graph.nodes.values().filter(|n| n.valve_flow != 0).count();
    println!("{}", nonzero_nodes);
    let mut product = 1;
    for i in 2..=nonzero_nodes {
        product *= i;
    }
    println!("{}", product);
    while graph.time_remaining > 0 {
        let node_distances = graph.distances_to_nodes();
        println!("{:?}", node_distances);
        for (name, distance) in node_distances {
            let potential_pressure_release =
                graph.nodes.get(name).unwrap().valve_flow * (graph.time_remaining - distance - 1);
            println!("{} {}", name, potential_pressure_release);
        }
        break;
    }
    println!("{:?}", graph.distances_to_nodes());
}
