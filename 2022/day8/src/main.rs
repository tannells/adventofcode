struct TreeVisitor {
    max_previous_height: i8,
}

impl TreeVisitor {
    fn next(&mut self, next_tree_height: i8) -> bool {
        // println!("next {} max {}", next_tree_height, self.max_previous_height);
        if next_tree_height > self.max_previous_height {
            self.max_previous_height = next_tree_height;
            return true;
        }
        return false;
    }

    fn reset(&mut self) {
        self.max_previous_height = -1;
    }
}

fn main() {
    let tree_heights: Vec<Vec<i8>> = include_str!("../params.txt")
        .lines()
        .map(|line| {
            line.chars()
                .map(|char| char.to_string().parse::<i8>().unwrap())
                .collect()
        })
        .collect();
    // println!("{:?}", tree_heights);
    let rows = tree_heights.len();
    let columns = tree_heights.first().unwrap().len();
    let mut tree_visibilities: Vec<Vec<bool>> = vec![vec![false; rows]; columns];

    // part 1
    let mut tv = TreeVisitor {
        max_previous_height: 0,
    };
    for row in 0..rows {
        tv.reset();
        for col in 0..columns {
            tree_visibilities[row][col] =
                tv.next(tree_heights[row][col]) || tree_visibilities[row][col];
        }
        tv.reset();
        for col in (0..columns).rev() {
            tree_visibilities[row][col] =
                tv.next(tree_heights[row][col]) || tree_visibilities[row][col];
        }
    }
    for col in 0..columns {
        tv.reset();
        for row in 0..rows {
            tree_visibilities[row][col] =
                tv.next(tree_heights[row][col]) || tree_visibilities[row][col];
        }
        tv.reset();
        for row in (0..rows).rev() {
            tree_visibilities[row][col] =
                tv.next(tree_heights[row][col]) || tree_visibilities[row][col];
        }
    }
    // print_tree_visibilities(&tree_visibilities);

    let visible_trees = tree_visibilities.iter().fold(0, |sum, row| {
        sum + row
            .iter()
            .fold(0, |sum, visible| sum + if *visible { 1 } else { 0 })
    });
    println!("visible trees {}", visible_trees);

    // part 2
    let mut scenic_scores: Vec<Vec<i32>> = vec![vec![0; rows]; columns];
    let mut max_scenic_score = 0;
    for starting_row in 0..rows {
        for starting_col in 0..columns {
            let mut starting_height = tree_heights[starting_row][starting_col];

            let mut up = 0;
            for col in (0..starting_col).rev() {
                up += 1;
                if tree_heights[starting_row][col] >= starting_height {
                    break;
                }
            }

            let mut down = 0;
            for col in (starting_col + 1)..columns {
                down += 1;
                if tree_heights[starting_row][col] >= starting_height {
                    break;
                }
            }

            //left
            let mut left = 0;
            for row in (0..starting_row).rev() {
                left += 1;
                if tree_heights[row][starting_col] >= starting_height {
                    break;
                }
            }

            //right
            starting_height = tree_heights[starting_row][starting_col];
            let mut right = 0;
            for row in (starting_row + 1)..rows {
                right += 1;
                if tree_heights[row][starting_col] >= starting_height {
                    break;
                }
            }
            let scenic_score = up * down * left * right;
            scenic_scores[starting_row][starting_col] = scenic_score;
            if scenic_score > max_scenic_score {
                max_scenic_score = scenic_score
            }
        }
    }
    // print_scenic_scores(&scenic_scores);
    println!("max scenic score {}", max_scenic_score);
}

fn print_tree_visibilities(tree_visibilities: &Vec<Vec<bool>>) {
    let rows = tree_visibilities.len();
    let columns = tree_visibilities.first().unwrap().len();
    for row in 0..rows {
        for col in 0..columns {
            print!("{}", if tree_visibilities[row][col] { 1 } else { 0 });
        }
        print!("\n");
    }
    print!("\n");
}

fn print_scenic_scores(scenic_scores: &Vec<Vec<i32>>) {
    let rows = scenic_scores.len();
    let columns = scenic_scores.first().unwrap().len();
    for row in 0..rows {
        for col in 0..columns {
            print!("{}\t", scenic_scores[row][col]);
        }
        print!("\n");
    }
    print!("\n");
}
