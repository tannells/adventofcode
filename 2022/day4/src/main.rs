fn main() {
    let pairs_with_contained_ranges = include_str!("../params.txt")
        .lines()
        .map(|pair| {
            pair.split(",")
                .map(|assignment| {
                    assignment
                        .split("-")
                        .map(|limit| limit.parse::<i32>().unwrap())
                        .collect::<Vec<i32>>()
                })
                .collect::<Vec<Vec<i32>>>()
        })
        .fold(0, |sum, pair| {
            if pair[0][0] >= pair[1][0] && pair[0][1] <= pair[1][1]
                || pair[0][0] <= pair[1][0] && pair[0][1] >= pair[1][1]
            {
                sum + 1
            } else {
                sum
            }
        });

    println!(
        "pairs with contained ranges: {}",
        pairs_with_contained_ranges
    );

    let pairs_with_overlapping_ranges = include_str!("../params.txt")
        .lines()
        .map(|pair| {
            pair.split(",")
                .map(|assignment| {
                    assignment
                        .split("-")
                        .map(|limit| limit.parse::<i32>().unwrap())
                        .collect::<Vec<i32>>()
                })
                .collect::<Vec<Vec<i32>>>()
        })
        .fold(0, |sum, pair| {
            if pair[0][1] >= pair[1][0] && pair[0][0] <= pair[1][1]
                || pair[0][1] <= pair[1][0] && pair[0][0] >= pair[1][1]
            {
                sum + 1
            } else {
                sum
            }
        });

    println!(
        "pairs with contained ranges: {}",
        pairs_with_overlapping_ranges
    );
}
