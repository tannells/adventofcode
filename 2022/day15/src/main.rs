use std::{cmp, collections::BTreeSet, ops::Range};

use lazy_static::lazy_static;
use regex::Regex;

#[derive(Debug)]
struct Point {
    x: i32,
    y: i32,
}

#[derive(Debug)]
struct Sensor {
    location: Point,
    nearest_beacon: Point,
}

impl Sensor {
    fn radius(&self) -> i32 {
        (self.location.x - self.nearest_beacon.x).abs()
            + (self.location.y - self.nearest_beacon.y).abs()
    }

    fn projection(&self, row: i32) -> Range<i32> {
        let distance_to_row = (self.location.y - row).abs();
        let overlap = self.radius() - distance_to_row;
        return (self.location.x - overlap)..(self.location.x + overlap + 1);
    }
}

trait Combinable {
    fn combine(&self, other: &Self) -> Vec<Self>
    where
        Self: Sized;
}

impl Combinable for Range<i32> {
    fn combine(&self, other: &Self) -> Vec<Self> {
        if self.end < other.start {
            // left
            return vec![self.clone(), other.clone()];
        } else if self.start > other.end {
            // right
            return vec![other.clone(), self.clone()];
        } else if self.start >= other.start && self.end <= other.end {
            // contained
            return vec![other.clone()];
        } else if self.start <= other.start && self.end >= other.end {
            // contains
            return vec![self.clone()];
        } else if self.start < other.start && self.end >= other.start {
            // overlap left
            return vec![self.start..other.end];
        } else if self.start >= other.start && self.end > other.end {
            // overlap right
            return vec![other.start..self.end];
        }
        return Vec::new();
    }
}

#[derive(Debug)]
struct RangeSet {
    ranges: Vec<Range<i32>>,
}

impl RangeSet {
    fn new() -> RangeSet {
        RangeSet { ranges: Vec::new() }
    }

    fn insert(&mut self, range: Range<i32>) {
        if range.is_empty() {
            return;
        }

        self.ranges.push(range);
        self.sort_ranges();
        self.combine_ranges();
    }

    fn sort_ranges(&mut self) {
        self.ranges.sort_by(|a, b| a.start.cmp(&b.start))
    }

    fn combine_ranges(&mut self) {
        if self.ranges.len() < 2 {
            return;
        }

        // println!("after insert {:?}", self.ranges);

        let mut new_ranges = Vec::new();
        let mut previous = 0..0;
        for i in 0..self.ranges.len() - 1 {
            let mut left = self.ranges[i].clone();
            let right = self.ranges[i + 1].clone();
            let mut combined_ranges = previous.combine(&left);
            // println!("previous left combine {:?}", combined_ranges);
            if combined_ranges.len() == 1 {
                left = combined_ranges[0].clone();
                new_ranges.pop();
            }
            combined_ranges = left.combine(&right);
            // println!("left right combine {:?}", combined_ranges);
            previous = combined_ranges.last().unwrap().clone();
            new_ranges.append(&mut combined_ranges);
            // println!("new_ranges {:?}", new_ranges);
        }
        self.ranges = new_ranges;
    }

    fn len(&self) -> usize {
        self.ranges
            .iter()
            .map(|range| range.len())
            .reduce(|sum, len| sum + len)
            .unwrap()
    }
}

fn main() {
    let example = include_str!("../example.txt");
    let params = include_str!("../params.txt");

    println!("{}", positions_without_beacons(example, 10));
    println!("{}", positions_without_beacons(params, 2000000));
    let mut position = distress_beacon_position(example, 0, 20);
    println!("{:?}", position);
    println!("tuning frequency {}", tuning_frequency(position));
    position = distress_beacon_position(params, 0, 4000000);
    println!("{:?}", position);
    println!("tuning frequency {}", tuning_frequency(position));
}

fn parse_sensors(string: &str) -> Vec<Sensor> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"x=(-?\d+), y=(-?\d+).*x=(-?\d+), y=(-?\d+)").unwrap();
    }

    RE.captures_iter(string)
        .map(|capture_group| Sensor {
            location: Point {
                x: capture_group[1].parse().unwrap(),
                y: capture_group[2].parse().unwrap(),
            },
            nearest_beacon: Point {
                x: capture_group[3].parse().unwrap(),
                y: capture_group[4].parse().unwrap(),
            },
        })
        .collect()
}

fn positions_without_beacons(input: &str, row: i32) -> usize {
    let sensors = parse_sensors(input);
    let mut beacon_positions = BTreeSet::new();
    let mut beacon_free_positions = RangeSet::new();
    for sensor in sensors {
        let projection = sensor.projection(row);
        // println!("{:?} {}", projection, projection.is_empty());
        beacon_free_positions.insert(projection);

        // println!("{:?}", beacon_free_positions);
        if sensor.nearest_beacon.y == row {
            beacon_positions.insert(sensor.nearest_beacon.x);
        }
    }

    return beacon_free_positions.len() - beacon_positions.len();
}

fn distress_beacon_position(input: &str, min: i32, max: i32) -> Point {
    let sensors = parse_sensors(input);
    let mut x = 0;
    let mut y = 0;
    for row in 0..=max {
        // println!("{}", row);
        let mut beacon_free_positions = RangeSet::new();
        for sensor in &sensors {
            let mut projection = sensor.projection(row);
            // limit search area
            projection.start = cmp::max(projection.start, min);
            projection.end = cmp::min(projection.end, max);
            beacon_free_positions.insert(projection)
        }
        if beacon_free_positions.len() < max as usize {
            x = beacon_free_positions.ranges[0].end;
            y = row;
            break;
        }
    }
    return Point { x: x, y: y };
}

fn tuning_frequency(point: Point) -> u64 {
    point.x as u64 * 4000000 + point.y as u64
}
