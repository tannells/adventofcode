fn main() {
    let lines = include_str!("../params.txt").lines();
    let mut sums = Vec::new();
    let mut sum = 0;
    for line in lines {
        if line != "" {
            sum += line.parse::<u32>().unwrap();
        } else {
            sums.push(sum);
            sum = 0;
        }
    }
    sums.sort_by(|a, b| b.cmp(a));
    sum = 0;
    for i in 0..=2 {
        sum += sums[i];
    }
    println!("{}", sum);
}
