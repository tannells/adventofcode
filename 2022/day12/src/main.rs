use std::collections::BTreeSet;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
struct Location {
    x: usize,
    y: usize,
}

impl Location {
    fn up(&self) -> Location {
        Location {
            x: self.x,
            y: self.y - 1,
        }
    }
    fn right(&self) -> Location {
        Location {
            x: self.x + 1,
            y: self.y,
        }
    }
    fn down(&self) -> Location {
        Location {
            x: self.x,
            y: self.y + 1,
        }
    }
    fn left(&self) -> Location {
        Location {
            x: self.x - 1,
            y: self.y,
        }
    }
}

#[derive(Debug)]
struct Map {
    grid: Vec<Vec<char>>,
    steps: Vec<Vec<i32>>,
    rows: usize,
    columns: usize,
    starting_location: Location,
    ending_location: Location,
}

impl Map {
    fn parse(input: &str) -> Map {
        let grid: Vec<Vec<char>> = input
            .lines()
            .map(|line| line.chars().collect::<Vec<char>>())
            .collect();
        let rows = grid.len();
        let columns = grid.first().unwrap().len();
        let steps = vec![vec![-1; columns]; rows];

        let mut starting_location = Location { x: 0, y: 0 };
        let mut ending_location = Location { x: 0, y: 0 };
        for row in 0..rows {
            for column in 0..columns {
                if grid[row][column] == 'S' {
                    starting_location = Location { x: column, y: row }
                }
                if grid[row][column] == 'E' {
                    ending_location = Location { x: column, y: row }
                }
            }
        }

        return Map {
            grid: grid,
            steps: steps,
            rows: rows,
            columns: columns,
            starting_location: starting_location,
            ending_location: ending_location,
        };
    }

    fn adjacent_locations(&self, location: &Location) -> Vec<Location> {
        let mut adjacent_locations = Vec::new();
        if location.x > 0 && self.reachable(location, &location.left()) {
            adjacent_locations.push(location.left())
        }
        if location.x < self.columns - 1 && self.reachable(location, &location.right()) {
            adjacent_locations.push(location.right())
        }
        if location.y > 0 && self.reachable(location, &location.up()) {
            adjacent_locations.push(location.up())
        }
        if location.y < self.rows - 1 && self.reachable(location, &location.down()) {
            adjacent_locations.push(location.down())
        }
        return adjacent_locations;
    }

    fn get_elevation(&self, location: &Location) -> u32 {
        let mut height = self.grid[location.y][location.x];
        if height == 'S' {
            height = 'a'
        }
        if height == 'E' {
            height = 'z'
        }
        u32::from(height)
    }

    fn reachable(&self, starting_location: &Location, next_location: &Location) -> bool {
        self.get_elevation(&starting_location) + 1 >= self.get_elevation(&next_location)
    }

    fn steps_to(&self, location: &Location) -> i32 {
        self.steps[location.y][location.x]
    }

    fn flood(&mut self, starting_locations: &mut BTreeSet<Location>) -> i32 {
        let mut edge_locations: BTreeSet<Location> = BTreeSet::new();
        edge_locations.append(starting_locations);
        let mut steps = 0;
        loop {
            // println!("steps {} edges {}", steps, edge_locations.len());
            // if edge_locations.is_empty() { return -1 }
            let mut next_edges: BTreeSet<Location> = BTreeSet::new();

            for edge in &edge_locations {
                self.steps[edge.y][edge.x] = steps;
            }
            for edge in &edge_locations {
                if edge == &self.ending_location {
                    return steps;
                }
                let mut new_edges = self
                    .adjacent_locations(&edge)
                    .into_iter()
                    .filter(|adjacent| self.steps_to(adjacent) == -1)
                    .collect();
                next_edges.append(&mut new_edges);
            }
            edge_locations.clear();
            edge_locations.append(&mut next_edges);
            steps += 1;
        }
    }

    fn reset_steps(&mut self) {
        self.steps = vec![vec![-1; self.columns]; self.rows];
    }

    fn print_steps(&self) {
        for row in self.steps.clone() {
            for steps in row {
                print!("{}\t", steps);
            }
            print!("\n");
        }
    }
}

fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let mut map = Map::parse(input);

    // flood
    let mut starting_locations = BTreeSet::new();
    starting_locations.insert(map.starting_location.clone());
    let from_start = map.flood(&mut starting_locations);
    println!("fewest steps from start to best signal = {}", from_start);
    // map.print_steps();

    for row in 0..map.rows {
        for column in 0..map.columns {
            if map.grid[row][column] == 'a' {
                starting_locations.insert(Location { x: column, y: row });
            }
        }
    }

    map.reset_steps();
    let from_lowest = map.flood(&mut starting_locations);
    println!("fewet steps from lowest to best signal = {}", from_lowest);
    // map.print_steps();
}
