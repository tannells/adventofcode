use std::collections::HashMap;

fn main() {
    let strategy_guide = include_str!("../params.txt");
    // println!("{}", strategy_guide);
    let rounds = strategy_guide.lines();

    let abc = ["A", "B", "C"];
    let xyz = ["X", "Y", "Z"];
    let rps = ["Rock", "Paper", "Scissors"];

    let mut move_lookup = HashMap::new();
    for i in 0..=2 {
        move_lookup.insert(abc[i], rps[i]);
    }

    let mut response_lookup = HashMap::new();
    for opponent_move in rps {
        let mut result = HashMap::new();
        for goal in 0..=2 {
            let opponent_index = rps.iter().position(|&x| x == opponent_move).unwrap() as i8;
            // rem_euclid is mod (least nonnegative remainder)
            let my_index = (opponent_index + goal - 1).rem_euclid(3);
            let my_move = rps[my_index as usize];
            result.insert(goal, my_move);
        }
        response_lookup.insert(opponent_move, result);
    }
    // println!("{:?}", response_lookup);

    let mut total_score = 0;
    for round in rounds {
        let moves = round.split(" ").collect::<Vec<_>>();
        let opponent_move = move_lookup.get(moves[0]).unwrap();
        // println!("{}", opponent_move);
        let my_goal = xyz.iter().position(|&x| x == moves[1]).unwrap();
        // println!("{}", my_response);
        let my_response = *response_lookup
            .get(opponent_move)
            .unwrap()
            .get(&(my_goal as i8))
            .unwrap();
        // println!("{}", my_response);
        let shape_score = rps.iter().position(|&x| x == my_response).unwrap() as i8 + 1;
        // println!("{}", shape_score);
        let outcome_score = my_goal * 3;
        // println!("{}", outcome_score);
        total_score += shape_score as i32 + outcome_score as i32;
    }

    println!("{}", total_score);
}
