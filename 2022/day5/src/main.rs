fn main() {
    // println!(include_str!("../example.txt"));

    let inputs: Vec<&str> = include_str!("../params.txt").split("\n\n").collect();
    // println!("{:?}", inputs);

    let crate_stacks = inputs[0];
    let rearrangement_procedure = inputs[1];

    let mut stack_lines = crate_stacks.lines().rev();

    let num_stacks = stack_lines
        .next()
        .unwrap()
        .split_whitespace()
        .last()
        .unwrap()
        .parse::<usize>()
        .unwrap();

    let mut stacks_a: Vec<Vec<char>> = Vec::new();
    let mut stacks_b: Vec<Vec<char>> = Vec::new();
    for _ in 0..num_stacks {
        stacks_a.push(Vec::new());
        stacks_b.push(Vec::new());
    }

    for line in stack_lines {
        let chars: Vec<char> = line.chars().collect();
        for i in 0..num_stacks {
            let letter = chars[4 * i + 1];
            if letter != ' ' {
                stacks_a[i].push(chars[4 * i + 1]);
                stacks_b[i].push(chars[4 * i + 1]);
            }
        }
    }
    // println!("{:?}", stacks);

    for line in rearrangement_procedure.lines() {
        let mut line_iter = line.split(" ");
        let number_to_move = line_iter.nth(1).unwrap().parse::<usize>().unwrap();
        let source = line_iter.nth(1).unwrap().parse::<usize>().unwrap() - 1;
        let destination = line_iter.nth(1).unwrap().parse::<usize>().unwrap() - 1;
        // println!("{} {} {}", number_to_move, source, destination);

        for _ in 0..number_to_move {
            let temp = stacks_a[source].pop().unwrap();
            stacks_a[destination].push(temp);
        }

        let mut temp_stack: Vec<char> = Vec::new();
        for _ in 0..number_to_move {
            temp_stack.push(stacks_b[source].pop().unwrap());
        }

        for _ in 0..number_to_move {
            stacks_b[destination].push(temp_stack.pop().unwrap());
        }
    }

    // println!("{:?}", stacks);
    let mut stack_tops_a: Vec<char> = Vec::new();
    let mut stack_tops_b: Vec<char> = Vec::new();
    for stack in stacks_a {
        stack_tops_a.push(*stack.last().unwrap());
    }
    for stack in stacks_b {
        stack_tops_b.push(*stack.last().unwrap());
    }
    println!("{}", stack_tops_a.into_iter().collect::<String>());
    println!("{}", stack_tops_b.into_iter().collect::<String>());
}
