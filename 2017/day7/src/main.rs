use lazy_static::lazy_static;
use regex::Regex;
use std::collections::{BTreeMap, BTreeSet};

use std::io::{stdin, stdout, Read, Write};

fn pause() {
    let mut stdout = stdout();
    stdout.write(b"Press Enter to continue...").unwrap();
    stdout.flush().unwrap();
    stdin().read(&mut [0]).unwrap();
}

#[derive(Debug)]
struct Tree {
    root_node: Node,
}

impl Tree {
    fn new() -> Tree {
        Tree {
            root_node: Node::new(),
        }
    }

    fn insert(&mut self, name: &str, weight: u32, child_list: &str) {
        if self.root_node.name == "" {
            self.root_node = Node {
                name: name.to_string(),
                weight: weight,
                children: Self::instantiate_children(child_list),
            }
        } else {
            match self.get_mut(name) {
                Some(child) => {
                    child.weight = weight;
                    child.children = Self::instantiate_children(child_list);
                }
                None => {}
            }
        }
    }

    fn instantiate_children(child_list: &str) -> BTreeMap<String, Node> {
        return child_list
            .split(", ")
            .map(|child_name| {
                (
                    child_name.to_string(),
                    Node {
                        name: child_name.to_string(),
                        weight: 0,
                        children: BTreeMap::new(),
                    },
                )
            })
            .collect();
    }

    fn get_mut(&mut self, name: &str) -> Option<&mut Node> {
        return Self::get_from_mut(&mut self.root_node, name);
    }

    fn get_from_mut<'n>(node: &'n mut Node, name: &str) -> Option<&'n mut Node> {
        if node.name == name {
            return Option::Some(node);
        } else {
            let val = node.children.get(name);
            match val {
                Some(_) => {
                    return node.children.get_mut(name);
                }
                None => {
                    for child in node.children.values_mut() {
                        match Self::get_from_mut(child, name) {
                            Some(child) => return Some(child),
                            None => {}
                        }
                    }
                    return None;
                }
            }
        }
    }

    fn get(&self, name: &str) -> Option<&Node> {
        return Self::get_from(&self.root_node, name);
    }

    fn get_from<'n>(node: &'n Node, name: &str) -> Option<&'n Node> {
        // println!("get from node {:?} name {}", node, name);
        // pause();
        if node.name == name {
            return Option::Some(node);
        } else {
            let val = node.children.get(name);
            match val {
                Some(_) => {
                    // println!("val {:?}", val);
                    return val;
                }
                None => {
                    for child in node.children.values() {
                        match Self::get_from(child, name) {
                            Some(child) => {
                                return Some(child);
                            }
                            None => {}
                        }
                    }
                }
            }
            return None;
        }
    }

    fn parse(input: &str) -> Tree {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"(\w*) \((\d*)\)(?: -> )?(\w*(?:, \w*)*)").unwrap();
        }

        let mut tree = Tree::new();

        let mut all_children = BTreeSet::new();
        let mut nodes = RE
            .captures_iter(input)
            .map(|capture| {
                let name = capture.get(1).unwrap().as_str();
                // println!("name {}", name);
                let weight = capture.get(2).unwrap().as_str().parse().unwrap();
                let child_list = match capture.get(3) {
                    Some(val) => val.as_str(),
                    None => "",
                };
                for child in child_list.split(", ") {
                    if child != "" {
                        all_children.insert(child);
                    }
                }
                // println!("{} {} {}", name, weight, child_list);
                (name, weight, child_list)
            })
            .collect::<Vec<(&str, u32, &str)>>();

        // println!("{:?}", all_children);
        let root_node_position = nodes
            .iter()
            .position(|node| all_children.get(node.0) == None)
            .unwrap();
        let root = nodes[root_node_position];
        println!("name of root {}", root.0);
        nodes.remove(root_node_position);
        tree.insert(root.0, root.1, root.2);
        // tree.root_node.insert_children(&mut tree, &mut nodes);

        while nodes.len() > 0 {
            let mut nodes_to_remove: Vec<usize> = Vec::new();
            for i in 0..nodes.len() {
                let node_params = nodes[i];
                let mut node = tree.get_mut(node_params.0);
                match node {
                    Some(val) => {
                        tree.insert(node_params.0, node_params.1, node_params.2);
                        nodes_to_remove.push(i);
                    }
                    None => {}
                }
            }
            nodes_to_remove.sort_by(|a, b| b.cmp(&a));
            for i in nodes_to_remove {
                nodes.remove(i);
            }
        }

        return tree;
    }

    fn find_unbalanced_node(&self) -> &Node {
        Self::find_unbalanced_node_from(&self.root_node).unwrap()
    }
    fn find_unbalanced_node_from(node: &Node) -> Option<&Node> {
        if node.balanced() {
            for child in node.children.values() {
                let val = Self::find_unbalanced_node_from(child);
                match val {
                    Some(_) => return val,
                    None => {}
                }
            }
            return None;
        } else {
            return Some(node);
        }
    }
}

#[derive(Debug)]
struct Node {
    name: String,
    children: BTreeMap<String, Node>,
    weight: u32,
}

impl Node {
    fn new() -> Node {
        Node {
            name: "".to_string(),
            children: BTreeMap::new(),
            weight: 0,
        }
    }

    fn child_weights(&self) -> Vec<u32> {
        return self
            .children
            .values()
            .map(|child| child.total_weight())
            .collect();
    }

    fn balanced(&self) -> bool {
        return self
            .child_weights()
            .into_iter()
            .collect::<BTreeSet<u32>>()
            .len()
            == 1;
    }

    fn total_weight(&self) -> u32 {
        return self.weight
            + self.children.values().fold(0, |total_weight, child_node| {
                total_weight + child_node.total_weight()
            });
    }
}

fn main() {
    let example = include_str!("../example.txt");
    let params = include_str!("../params.txt");

    run(example);
    run(params);

    // println!("{:?}", build_tree(example));
    // println!("{:?}", build_tree(params));
}

fn run(input: &str) {
    let tree = Tree::parse(input);
    let unbalanced_node = tree.find_unbalanced_node();

    let mut child_iter = unbalanced_node.children.values();
    let mut unbalanced_child = child_iter.next().unwrap();

    let mut target_weight = 0;
    let child1 = unbalanced_child;
    let child2 = child_iter.next().unwrap();

    println!(
        "{} {} {:?}",
        unbalanced_node.name,
        unbalanced_node.weight,
        unbalanced_node.child_weights()
    );

    if child1.total_weight() != child2.total_weight() {
        let child3 = child_iter.next().unwrap();
        if child1.total_weight() == child3.total_weight() {
            unbalanced_child = child2;
            target_weight = child1.total_weight();
        } else {
            unbalanced_child = child1;
            target_weight = child2.total_weight();
        }
    } else {
        target_weight = child1.total_weight();
        for child in child_iter {
            if child.total_weight() != child1.total_weight() {
                unbalanced_child = child;
                break;
            }
        }
    }

    let unbalanced_weight = unbalanced_child.total_weight();
    let diff = target_weight as i32 - unbalanced_weight as i32;

    println!(
        "name: {} weight: {} target_weight: {} adjusted_weight: {}",
        unbalanced_child.name,
        unbalanced_child.weight,
        unbalanced_child.total_weight(),
        unbalanced_child.weight as i32 + diff
    );
}
