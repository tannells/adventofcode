use std::collections::BTreeSet;

fn main() {
    let input = include_str!("../params.txt");
    println!("{}", count_valid_passwords1(input));
    println!("{}", count_valid_passwords2(input));
}

fn count_valid_passwords1(input: &str) -> usize {
    let mut sum: usize = 0;
    for line in input.lines() {
        let words: Vec<String> = line.split(" ").map(|str| str.to_string()).collect();
        let word_count = words.len();
        let unique_words = words.into_iter().collect::<BTreeSet<String>>();
        if unique_words.len() == word_count {
            sum += 1;
        }
    }
    return sum;
}

fn count_valid_passwords2(input: &str) -> usize {
    let mut sum: usize = 0;
    for line in input.lines() {
        let words: Vec<String> = line
            .split(" ")
            .map(|word| {
                let mut word_chars = word.chars().collect::<Vec<char>>();
                word_chars.sort();
                return word_chars.into_iter().collect::<String>();
            })
            .collect::<Vec<String>>();
        let word_count = words.len();
        let unique_words = words.into_iter().collect::<BTreeSet<String>>();
        if unique_words.len() == word_count {
            sum += 1;
        }
    }
    return sum;
}
