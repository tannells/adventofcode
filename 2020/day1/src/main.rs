use itertools::Itertools;
fn main() {
    let expense_report = include_str!("../params.txt");

    let entries = expense_report
        .lines()
        .map(|line| line.parse::<u32>().unwrap())
        .collect::<Vec<u32>>();

    // quick solution
    'outer: for i in 0..entries.len() {
        for j in 0..entries.len() {
            if i == j {
                continue;
            }
            if entries[i] + entries[j] == 2020 {
                println!(
                    "{} * {} = {}",
                    entries[i],
                    entries[j],
                    entries[i] * entries[j]
                );
                break 'outer;
            }
        }
    }

    'outer: for i in 0..entries.len() {
        for j in 0..entries.len() {
            for k in 0..entries.len() {
                if i == j || i == k || j == k {
                    continue;
                }
                if entries[i] + entries[j] + entries[k] == 2020 {
                    println!(
                        "{} * {} * {} = {}",
                        entries[i],
                        entries[j],
                        entries[k],
                        entries[i] * entries[j] * entries[k]
                    );
                    break 'outer;
                }
            }
        }
    }

    // solution to find any n numbers that have a given sum
    let values = find_n_with_sum(&entries, 2, 2020);
    println!(
        "{} * {} = {}",
        values[0],
        values[1],
        values.iter().copied().product::<u32>()
    );

    let values = find_n_with_sum(&entries, 3, 2020);
    println!(
        "{} * {} * {} = {}",
        values[0],
        values[1],
        values[2],
        values.iter().copied().product::<u32>()
    );
}

fn find_n_with_sum(entries: &Vec<u32>, n: usize, sum: u32) -> Vec<&u32> {
    return entries
        .iter()
        .combinations(n)
        .find(|combination| combination.iter().copied().sum::<u32>() == sum)
        .unwrap();
}
