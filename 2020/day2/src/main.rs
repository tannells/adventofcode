use lazy_regex::regex_captures;

fn main() {
    let passwords_list = include_str!("../params.txt");

    // validator defined as a closure
    let validator_1 = |min: usize, max: usize, character: char, password: &str| -> bool {
        let count = password
            .chars()
            .fold(0, |count, c| if c == character { count + 1 } else { count });
        min <= count && count <= max
    };

    let mut valid_passwords = count_valid_passwords(passwords_list, validator_1);
    println!("Part 1 - valid passwords: {}", valid_passwords);

    valid_passwords = count_valid_passwords(passwords_list, validator_2);
    println!("Part 2 - valid passwords: {}", valid_passwords);
}

fn count_valid_passwords(
    passwords_list: &str,
    validator: fn(usize, usize, char, &str) -> bool,
) -> usize {
    passwords_list.lines().fold(0, |valid_passwords, line| {
        let (_whole, first_s, second_s, character, password) =
            regex_captures!(r#"^(\d+)-(\d+) (.): (.*)$"#, line).unwrap();
        let first = first_s.parse::<usize>().unwrap();
        let second = second_s.parse::<usize>().unwrap();
        let required_char = character.chars().next().unwrap();
        if validator(first, second, required_char, password) {
            valid_passwords + 1
        } else {
            valid_passwords
        }
    })
}

// validator defined as a function
fn validator_2(index1: usize, index2: usize, character: char, password: &str) -> bool {
    let char_1 = password.chars().nth(index1 - 1).unwrap();
    let char_2 = password.chars().nth(index2 - 1).unwrap();
    return (char_1 == character) ^ (char_2 == character);
}
