use shared::parse_numbers;

fn main() {
    println!("Hi mom!");

    // parse numbers from a string
    let numbers = parse_numbers::<i32>("a1b2c34dfg56");
    println!("Parse numbers: {:?}", numbers);
}
