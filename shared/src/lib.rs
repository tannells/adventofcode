use lazy_regex::regex;
use std::fmt::Debug;
use std::str::FromStr;

pub fn parse_numbers<T: FromStr>(string: &str) -> Vec<T>
where
    <T as FromStr>::Err: Debug,
{
    let regex = regex!(r#"(\d+)"#);
    let numbers = regex
        .find_iter(string)
        .map(|m| m.as_str().parse::<T>().unwrap())
        .collect::<Vec<T>>();
    return numbers;
}
