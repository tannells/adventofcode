use std::collections::BTreeSet;

use lazy_regex::regex_captures;

fn main() {
    // let input = include_str!("../example.txt");
    let input = include_str!("../params.txt");

    let mut path = Path::new();

    for turn_steps in input.split(", ") {
        // println!("{}", turn_steps);
        let (_, turn, steps) = regex_captures!(r#"(R|L)(\d+)"#, turn_steps).unwrap();
        // println!("{}, {}", turn, steps);
        path.step(turn, steps.parse::<i32>().unwrap(), false);
    }

    println!("Part 1 - x {} y {} distance {}", path.x, path.y, path.x.abs() + path.y.abs());

    path = Path::new();
    for turn_steps in input.split(", ") {
        // println!("{}", turn_steps);
        let (_, turn, steps) = regex_captures!(r#"(R|L)(\d+)"#, turn_steps).unwrap();
        // println!("{}, {}", turn, steps);
        if !path.step(turn, steps.parse::<i32>().unwrap(), true) {
            break;
        }
    }

    println!("Part 2 - x {} y {} distance {}", path.x, path.y, path.x.abs() + path.y.abs())
}

struct Path {
    x: i32,
    y: i32,
    dir: i32,
    previous_locations: BTreeSet<(i32, i32)>
}

impl Path {
    fn new() -> Path {
        Path{
            x: 0,
            y: 0,
            dir: 0,
            previous_locations: BTreeSet::new()
        }
    }

    fn step(&mut self, turn: &str, steps: i32, stop_after_revisit: bool) -> bool {
        match turn {
            "R" => {
                self.dir += 1;
            },
            "L" => {
                self.dir -= 1;
            },
            _ => {}
        }
        // -1 % 4 -> -1, so we want rem_euclid
        self.dir = self.dir.rem_euclid(4);
        // println!("dir {}", self.dir);

        let mut result = true;
        for _ in 0..steps {
            result = match self.dir {
                0 => {
                    // north
                    self.y += 1;
                    self.add_current()
                },
                1 => {
                    // east
                    self.x += 1;
                    self.add_current()
                },
                2 => {
                    self.y -= 1;
                    self.add_current()
                },
                3 => {
                    self.x -= 1;
                    self.add_current()
                },
                _ => { false }
            };
            if !result && stop_after_revisit {
                break
            }
        }

        result
    }

    fn add_current(&mut self) -> bool {
        self.previous_locations.insert((self.x, self.y))
    }
}